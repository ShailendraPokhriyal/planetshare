package com.planetshare.retrofit;

import com.planetshare.models.plans.PlansResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface PlanApiInterface {
    @FormUrlEncoded
    @POST("get/packs")
    Call<PlansResponse> getPlanResponse(
            @Field("user_id") int user_id,
            @Field("pack_type") String pack_type,
            @Header("Authorization") String authKey,
            @Field("tag") String tag
    );
}
