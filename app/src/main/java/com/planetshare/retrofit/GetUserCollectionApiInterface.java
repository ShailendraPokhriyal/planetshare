package com.planetshare.retrofit;

import com.planetshare.models.collection.usercollection.UserCollectionListResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface GetUserCollectionApiInterface {

    @FormUrlEncoded
    @POST("get/collectionData")
    Call<UserCollectionListResponse> getUserCollectionApiResponse(
            @Field("user_id") int user_id,
            @Field("collection_type") String collection_type,
            @Field("collection_id") int collection_id,
            @Field("tag") String tag,
            @Header("Authorization") String authKey
    );
}
