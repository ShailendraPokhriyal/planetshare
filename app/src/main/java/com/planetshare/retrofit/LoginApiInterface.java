package com.planetshare.retrofit;

import com.planetshare.models.login.LoginResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface LoginApiInterface {
    @FormUrlEncoded
    @POST("login")
    Call<LoginResponse> getLoginResponse(
            @Field("email") String email,
            @Field("password") String password,
            @Field("tag") String tag
    );
   /*@POST("login/facebook")
    Call<FacebookResponse> getFacebookResponse(
            @Field("provider_name") String provider_name,
            @Field("provider_id") String provider_id,
            @Field("name") String name,
            @Field("email") String email,
            @Field("tag") String tag
    );*/
}
