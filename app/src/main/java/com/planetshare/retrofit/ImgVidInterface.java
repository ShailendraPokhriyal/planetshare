package com.planetshare.retrofit;

import com.planetshare.models.imagevideodetails.imgdetail.ImageResponse;
import com.planetshare.models.imagevideodetails.viddetail.VidDetailResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ImgVidInterface {
    /*ImgDetail*/
    @FormUrlEncoded
    @POST("get/imageData")
    Call<ImageResponse> getImgResponse(
            @Field("user_id") int user_id,
            @Field("image_id") String image_id,
            @Field("tag") String tag
    );

    /*VidDetails*/
    @FormUrlEncoded
    @POST("get/videoData")
    Call<VidDetailResponse> getVideoResponse(
            @Field("user_id") int user_id,
            @Field("video_id") String video_id,
            @Field("tag") String tag
    );
}
