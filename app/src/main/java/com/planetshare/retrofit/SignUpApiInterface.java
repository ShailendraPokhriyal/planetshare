package com.planetshare.retrofit;

import com.planetshare.models.signupemail.SignupResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface SignUpApiInterface {
    @FormUrlEncoded
    @POST("register")
    Call<SignupResponse> getSignUpResponse(
            @Field("name") String name,
            @Field("email") String email,
            @Field("password") String password,
            @Field("tag") String tag
    );
}
