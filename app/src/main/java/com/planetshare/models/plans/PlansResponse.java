
package com.planetshare.models.plans;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("unused")
public class PlansResponse implements Serializable {
    @SerializedName("data")
    private ArrayList<PlanData> planData;
    @SerializedName("msg")
    private String msg;
    @SerializedName("success")
    private int success;

    public ArrayList<PlanData> getPlanData() {
        return planData;
    }

    public void setPlanData(ArrayList<PlanData> data) {
        this.planData = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

}
