
package com.planetshare.models.plans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("unused")
public class PlanData implements Serializable {
    @SerializedName("content_type")
    private String contentType;
    @SerializedName("created_at")
    private String createdAt;
    @Expose
    private int id;
    @SerializedName("pack_count")
    private int packCount;
    @SerializedName("pack_description")
    private String packDescription;
    @SerializedName("pack_expire_time")
    private int packExpireTime;
    @SerializedName("pack_image")
    private String packImage;
    @SerializedName("pack_name")
    private String packName;
    @SerializedName("pack_price")
    private int packPrice;
    @SerializedName("pack_type")
    private String packType;
    @SerializedName("popular_content")
    private int popularContent;
    @SerializedName("request_id")
    private String requestId;
    @SerializedName("single_pack")
    private int singlePack;
    @Expose
    private int status;
    @SerializedName("updated_at")
    private String updatedAt;

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPackCount() {
        return packCount;
    }

    public void setPackCount(int packCount) {
        this.packCount = packCount;
    }

    public String getPackDescription() {
        return packDescription;
    }

    public void setPackDescription(String packDescription) {
        this.packDescription = packDescription;
    }

    public int getPackExpireTime() {
        return packExpireTime;
    }

    public void setPackExpireTime(int packExpireTime) {
        this.packExpireTime = packExpireTime;
    }

    public String getPackImage() {
        return packImage;
    }

    public void setPackImage(String packImage) {
        this.packImage = packImage;
    }

    public String getPackName() {
        return packName;
    }

    public void setPackName(String packName) {
        this.packName = packName;
    }

    public int getPackPrice() {
        return packPrice;
    }

    public void setPackPrice(int packPrice) {
        this.packPrice = packPrice;
    }

    public String getPackType() {
        return packType;
    }

    public void setPackType(String packType) {
        this.packType = packType;
    }

    public int getPopularContent() {
        return popularContent;
    }

    public void setPopularContent(int popularContent) {
        this.popularContent = popularContent;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public int getSinglePack() {
        return singlePack;
    }

    public void setSinglePack(int singlePack) {
        this.singlePack = singlePack;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

}
