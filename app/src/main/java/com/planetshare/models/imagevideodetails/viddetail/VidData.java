
package com.planetshare.models.imagevideodetails.viddetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


@SuppressWarnings("unused")
public class VidData implements Serializable {

    @SerializedName("artist_name")
    private String artistName;
    @Expose
    private String cast;
    @SerializedName("demo_url")
    private String demoUrl;
    @Expose
    private String description;
    @Expose
    private String dimension;
    @Expose
    private String director;
    @Expose
    private String extension;
    @Expose
    private String genre;
    @SerializedName("get_video_cat")
    private GetVideoCat getVideoCat;
    @Expose
    private int id;
    @Expose
    private String keywords;
    @SerializedName("language_id")
    private String languageId;
    @SerializedName("large_thumb")
    private String largeThumb;
    @SerializedName("license_rights")
    private String licenseRights;
    @SerializedName("mature_content")
    private int matureContent;
    @Expose
    private int premium;
    @Expose
    private int price;
    @Expose
    private String producer;
    @SerializedName("profile_pic")
    private String profilePic;
    @SerializedName("quality_url")
    private String qualityUrl;
    @SerializedName("request_id")
    private String requestId;
    @Expose
    private String resolution;
    @SerializedName("seller_id")
    private int sellerId;
    @SerializedName("seller_name")
    private String sellerName;
    @SerializedName("short_desc")
    private String shortDesc;
    @Expose
    private String size;
    @Expose
    private String tag;
    @Expose
    private String title;
    @SerializedName("user_id")
    private int userId;
    @SerializedName("video_category_id")
    private int videoCategoryId;
    @SerializedName("video_length")
    private String videoLength;

    public String getArtistName() {
        return artistName;
    }

    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }

    public String getCast() {
        return cast;
    }

    public void setCast(String cast) {
        this.cast = cast;
    }

    public String getDemoUrl() {
        return demoUrl;
    }

    public void setDemoUrl(String demoUrl) {
        this.demoUrl = demoUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDimension() {
        return dimension;
    }

    public void setDimension(String dimension) {
        this.dimension = dimension;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public GetVideoCat getGetVideoCat() {
        return getVideoCat;
    }

    public void setGetVideoCat(GetVideoCat getVideoCat) {
        this.getVideoCat = getVideoCat;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getLanguageId() {
        return languageId;
    }

    public void setLanguageId(String languageId) {
        this.languageId = languageId;
    }

    public String getLargeThumb() {
        return largeThumb;
    }

    public void setLargeThumb(String largeThumb) {
        this.largeThumb = largeThumb;
    }

    public String getLicenseRights() {
        return licenseRights;
    }

    public void setLicenseRights(String licenseRights) {
        this.licenseRights = licenseRights;
    }

    public int getMatureContent() {
        return matureContent;
    }

    public void setMatureContent(int matureContent) {
        this.matureContent = matureContent;
    }

    public int getPremium() {
        return premium;
    }

    public void setPremium(int premium) {
        this.premium = premium;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getQualityUrl() {
        return qualityUrl;
    }

    public void setQualityUrl(String qualityUrl) {
        this.qualityUrl = qualityUrl;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getResolution() {
        return resolution;
    }

    public void setResolution(String resolution) {
        this.resolution = resolution;
    }

    public int getSellerId() {
        return sellerId;
    }

    public void setSellerId(int sellerId) {
        this.sellerId = sellerId;
    }

    public String getSellerName() {
        return sellerName;
    }

    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }

    public String getShortDesc() {
        return shortDesc;
    }

    public void setShortDesc(String shortDesc) {
        this.shortDesc = shortDesc;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getVideoCategoryId() {
        return videoCategoryId;
    }

    public void setVideoCategoryId(int videoCategoryId) {
        this.videoCategoryId = videoCategoryId;
    }

    public String getVideoLength() {
        return videoLength;
    }

    public void setVideoLength(String videoLength) {
        this.videoLength = videoLength;
    }

}
