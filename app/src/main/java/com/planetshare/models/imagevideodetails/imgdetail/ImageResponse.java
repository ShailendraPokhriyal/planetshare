
package com.planetshare.models.imagevideodetails.imgdetail;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("unused")
public class ImageResponse implements Serializable {
    @SerializedName("cart_status")
    private int cartStatus;
    @SerializedName("collection_list")
    private ArrayList<CollectionListImg> collectionList;
    @SerializedName("data")
    private ImageData data;
    @SerializedName("pack_status_check")
    private int packStatusCheck;
    @SerializedName("similar_data")
    private ArrayList<SimilarDataImage> similarData;
    @SerializedName("success")
    private int success;

    public int getCartStatus() {
        return cartStatus;
    }

    public void setCartStatus(int cartStatus) {
        this.cartStatus = cartStatus;
    }

    public ArrayList<CollectionListImg> getCollectionList() {
        return collectionList;
    }

    public void setCollectionList(ArrayList<CollectionListImg> collectionList) {
        this.collectionList = collectionList;
    }

    public ImageData getData() {
        return data;
    }

    public void setData(ImageData data) {
        this.data = data;
    }

    public int getPackStatusCheck() {
        return packStatusCheck;
    }

    public void setPackStatusCheck(int packStatusCheck) {
        this.packStatusCheck = packStatusCheck;
    }

    public ArrayList<SimilarDataImage> getSimilarData() {
        return similarData;
    }

    public void setSimilarData(ArrayList<SimilarDataImage> similarData) {
        this.similarData = similarData;
    }

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

}
