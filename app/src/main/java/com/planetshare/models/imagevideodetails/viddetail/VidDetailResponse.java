
package com.planetshare.models.imagevideodetails.viddetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("unused")
public class VidDetailResponse implements Serializable {

    @SerializedName("cart_status")
    private int cartStatus;
    @SerializedName("collection_list")
    private ArrayList<CollectionListVid> collectionList;
    @Expose
    private VidData data;
    @SerializedName("pack_status_check")
    private int packStatusCheck;
    @SerializedName("similar_data")
    private ArrayList<SimilarDataVid> similarData;
    @Expose
    private int success;

    public int getCartStatus() {
        return cartStatus;
    }

    public void setCartStatus(int cartStatus) {
        this.cartStatus = cartStatus;
    }

    public ArrayList<CollectionListVid> getCollectionList() {
        return collectionList;
    }

    public void setCollectionList(ArrayList<CollectionListVid> collectionList) {
        this.collectionList = collectionList;
    }

    public VidData getData() {
        return data;
    }

    public void setData(VidData data) {
        this.data = data;
    }

    public int getPackStatusCheck() {
        return packStatusCheck;
    }

    public void setPackStatusCheck(int packStatusCheck) {
        this.packStatusCheck = packStatusCheck;
    }

    public ArrayList<SimilarDataVid> getSimilarData() {
        return similarData;
    }

    public void setSimilarData(ArrayList<SimilarDataVid> similarData) {
        this.similarData = similarData;
    }

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

}
