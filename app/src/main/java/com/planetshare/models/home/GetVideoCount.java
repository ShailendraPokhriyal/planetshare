
package com.planetshare.models.home;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("unused")
public class GetVideoCount implements Serializable {

    @SerializedName("artist_name")
    private String artistName;
    @Expose
    private String cast;
    @Expose
    private String description;
    @Expose
    private String dimension;
    @Expose
    private String extension;
    @Expose
    private int id;
    @Expose
    private String keywords;
    @SerializedName("large_thumb")
    private String largeThumb;
    @SerializedName("medium_thumb")
    private String mediumThumb;
    @Expose
    private int premium;
    @Expose
    private int price;
    @SerializedName("seller_id")
    private int sellerId;
    @SerializedName("short_desc")
    private String shortDesc;
    @Expose
    private String size;
    @SerializedName("small_thumb")
    private String smallThumb;
    @Expose
    private int status;
    @Expose
    private String title;
    @SerializedName("user_id")
    private int userId;
    @SerializedName("video_category_id")
    private int videoCategoryId;

    public String getArtistName() {
        return artistName;
    }

    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }

    public String getCast() {
        return cast;
    }

    public void setCast(String cast) {
        this.cast = cast;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDimension() {
        return dimension;
    }

    public void setDimension(String dimension) {
        this.dimension = dimension;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getLargeThumb() {
        return largeThumb;
    }

    public void setLargeThumb(String largeThumb) {
        this.largeThumb = largeThumb;
    }

    public String getMediumThumb() {
        return mediumThumb;
    }

    public void setMediumThumb(String mediumThumb) {
        this.mediumThumb = mediumThumb;
    }

    public int getPremium() {
        return premium;
    }

    public void setPremium(int premium) {
        this.premium = premium;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getSellerId() {
        return sellerId;
    }

    public void setSellerId(int sellerId) {
        this.sellerId = sellerId;
    }

    public String getShortDesc() {
        return shortDesc;
    }

    public void setShortDesc(String shortDesc) {
        this.shortDesc = shortDesc;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getSmallThumb() {
        return smallThumb;
    }

    public void setSmallThumb(String smallThumb) {
        this.smallThumb = smallThumb;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getVideoCategoryId() {
        return videoCategoryId;
    }

    public void setVideoCategoryId(int videoCategoryId) {
        this.videoCategoryId = videoCategoryId;
    }

}
