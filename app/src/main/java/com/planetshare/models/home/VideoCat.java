
package com.planetshare.models.home;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("unused")
public class VideoCat implements Serializable {

    @SerializedName("cat_name")
    private String catName;
    @Expose
    private String desc;
    @SerializedName("get_video_count_1")
    private ArrayList<GetVideoCount> getVideoCount;
    @SerializedName("get_video_count_1_count")
    private int getVideoCount1Count;
    @Expose
    private int id;
    @SerializedName("large_img")
    private String largeImg;
    @SerializedName("medium_img")
    private String mediumImg;
    @Expose
    private String name;
    @SerializedName("small_img")
    private String smallImg;

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public ArrayList<GetVideoCount> getGetVideoCount() {
        return getVideoCount;
    }

    public void setGetVideoCount(ArrayList<GetVideoCount> getVideoCount) {
        this.getVideoCount = getVideoCount;
    }

    public int getGetVideoCount1Count() {
        return getVideoCount1Count;
    }

    public void setGetVideoCount1Count(int getVideoCount1Count) {
        this.getVideoCount1Count = getVideoCount1Count;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLargeImg() {
        return largeImg;
    }

    public void setLargeImg(String largeImg) {
        this.largeImg = largeImg;
    }

    public String getMediumImg() {
        return mediumImg;
    }

    public void setMediumImg(String mediumImg) {
        this.mediumImg = mediumImg;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSmallImg() {
        return smallImg;
    }

    public void setSmallImg(String smallImg) {
        this.smallImg = smallImg;
    }

}
