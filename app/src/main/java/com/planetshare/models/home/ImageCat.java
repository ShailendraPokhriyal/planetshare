
package com.planetshare.models.home;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("unused")
public class ImageCat implements Serializable {

    @SerializedName("cat_name")
    private String catName;
    @Expose
    private String desc;
    @SerializedName("get_image_count_1")
    private ArrayList<GetImageCount> getImageCount;
    @SerializedName("get_image_count_1_count")
    private int getImageCount1Count;
    @Expose
    private int id;
    @SerializedName("large_img")
    private String largeImg;
    @SerializedName("medium_img")
    private String mediumImg;
    @Expose
    private String name;
    @SerializedName("small_img")
    private String smallImg;

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public ArrayList<GetImageCount> getGetImageCount() {
        return getImageCount;
    }

    public void setGetImageCount(ArrayList<GetImageCount> getImageCount) {
        this.getImageCount = getImageCount;
    }

    public int getGetImageCount1Count() {
        return getImageCount1Count;
    }

    public void setGetImageCount1Count(int getImageCount1Count) {
        this.getImageCount1Count = getImageCount1Count;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLargeImg() {
        return largeImg;
    }

    public void setLargeImg(String largeImg) {
        this.largeImg = largeImg;
    }

    public String getMediumImg() {
        return mediumImg;
    }

    public void setMediumImg(String mediumImg) {
        this.mediumImg = mediumImg;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSmallImg() {
        return smallImg;
    }

    public void setSmallImg(String smallImg) {
        this.smallImg = smallImg;
    }

}
