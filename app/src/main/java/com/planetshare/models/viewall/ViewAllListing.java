
package com.planetshare.models.viewall;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("unused")
public class ViewAllListing implements Serializable {

    @SerializedName("artist_name")
    private String artistName;
    @Expose
    private String cast;
    @Expose
    private String description;
    @Expose
    private String dimension;
    @Expose
    private String director;
    @Expose
    private String extension;
    @Expose
    private String genre;
    @Expose
    private int id;
    @Expose
    private String keywords;
    @SerializedName("language_id")
    private String languageId;
    @SerializedName("large_thumb")
    private String largeThumb;
    @SerializedName("license_rights")
    private String licenseRights;
    @SerializedName("mature_content")
    private int matureContent;
    @SerializedName("medium_thumb")
    private String mediumThumb;
    @Expose
    private int premium;
    @Expose
    private int price;
    @Expose
    private String producer;
    @SerializedName("profile_pic")
    private String profilePic;
    @SerializedName("quality_url")
    private String qualityUrl;
    @Expose
    private String resolution;
    @SerializedName("seller_id")
    private int sellerId;
    @SerializedName("seller_name")
    private String sellerName;
    @SerializedName("short_desc")
    private String shortDesc;
    @Expose
    private String size;
    @SerializedName("small_thumb")
    private String smallThumb;
    @Expose
    private int status;
    @SerializedName("term_condition")
    private int termCondition;
    @SerializedName("territory_rights")
    private String territoryRights;
    @Expose
    private String title;
    @SerializedName("total_buy")
    private int totalBuy;
    @SerializedName("user_id")
    private int userId;
    @SerializedName("video_category_id")
    private int videoCategoryId;
    @SerializedName("video_length")
    private String videoLength;

    public String getArtistName() {
        return artistName;
    }

    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }

    public String getCast() {
        return cast;
    }

    public void setCast(String cast) {
        this.cast = cast;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDimension() {
        return dimension;
    }

    public void setDimension(String dimension) {
        this.dimension = dimension;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getLanguageId() {
        return languageId;
    }

    public void setLanguageId(String languageId) {
        this.languageId = languageId;
    }

    public String getLargeThumb() {
        return largeThumb;
    }

    public void setLargeThumb(String largeThumb) {
        this.largeThumb = largeThumb;
    }

    public String getLicenseRights() {
        return licenseRights;
    }

    public void setLicenseRights(String licenseRights) {
        this.licenseRights = licenseRights;
    }

    public int getMatureContent() {
        return matureContent;
    }

    public void setMatureContent(int matureContent) {
        this.matureContent = matureContent;
    }

    public String getMediumThumb() {
        return mediumThumb;
    }

    public void setMediumThumb(String mediumThumb) {
        this.mediumThumb = mediumThumb;
    }

    public int getPremium() {
        return premium;
    }

    public void setPremium(int premium) {
        this.premium = premium;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getQualityUrl() {
        return qualityUrl;
    }

    public void setQualityUrl(String qualityUrl) {
        this.qualityUrl = qualityUrl;
    }

    public String getResolution() {
        return resolution;
    }

    public void setResolution(String resolution) {
        this.resolution = resolution;
    }

    public int getSellerId() {
        return sellerId;
    }

    public void setSellerId(int sellerId) {
        this.sellerId = sellerId;
    }

    public String getSellerName() {
        return sellerName;
    }

    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }

    public String getShortDesc() {
        return shortDesc;
    }

    public void setShortDesc(String shortDesc) {
        this.shortDesc = shortDesc;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getSmallThumb() {
        return smallThumb;
    }

    public void setSmallThumb(String smallThumb) {
        this.smallThumb = smallThumb;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getTermCondition() {
        return termCondition;
    }

    public void setTermCondition(int termCondition) {
        this.termCondition = termCondition;
    }

    public String getTerritoryRights() {
        return territoryRights;
    }

    public void setTerritoryRights(String territoryRights) {
        this.territoryRights = territoryRights;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getTotalBuy() {
        return totalBuy;
    }

    public void setTotalBuy(int totalBuy) {
        this.totalBuy = totalBuy;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getVideoCategoryId() {
        return videoCategoryId;
    }

    public void setVideoCategoryId(int videoCategoryId) {
        this.videoCategoryId = videoCategoryId;
    }

    public String getVideoLength() {
        return videoLength;
    }

    public void setVideoLength(String videoLength) {
        this.videoLength = videoLength;
    }

}
