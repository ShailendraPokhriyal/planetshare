
package com.planetshare.models.collection.getusercollection;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("unused")
public class UserPicture implements Serializable {
    @Expose
    private int id;
    @SerializedName("large_thumb")
    private String largeThumb;
    @Expose
    private int price;
    @SerializedName("request_id")
    private String requestId;
    @SerializedName("short_desc")
    private String shortDesc;
    @Expose
    private String title;
     public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLargeThumb() {
        return largeThumb;
    }

    public void setLargeThumb(String largeThumb) {
        this.largeThumb = largeThumb;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getShortDesc() {
        return shortDesc;
    }

    public void setShortDesc(String shortDesc) {
        this.shortDesc = shortDesc;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
