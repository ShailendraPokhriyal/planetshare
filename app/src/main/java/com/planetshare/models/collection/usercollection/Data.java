
package com.planetshare.models.collection.usercollection;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Data implements Serializable {

    @SerializedName("created_at")
    private String createdAt;
    @SerializedName("id")
    private int id;
    @SerializedName("image_item_info")
    private ImageItemInfo imageItemInfo;
    @SerializedName("video_item_info")
    private ImageItemInfo videoItemInfo;
    @SerializedName("item_id")
    private int itemId;
    @SerializedName("item_type")
    private String itemType;
    @SerializedName("request_id")
    private String requestId;

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ImageItemInfo getImageItemInfo() {
        return imageItemInfo;
    }

    public void setImageItemInfo(ImageItemInfo imageItemInfo) {
        this.imageItemInfo = imageItemInfo;
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public ImageItemInfo getVideoItemInfo() {
        return videoItemInfo;
    }

    public void setVideoItemInfo(ImageItemInfo videoItemInfo) {
        this.videoItemInfo = videoItemInfo;
    }
}
