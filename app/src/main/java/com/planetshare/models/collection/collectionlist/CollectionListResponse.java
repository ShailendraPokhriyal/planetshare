
package com.planetshare.models.collection.collectionlist;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("unused")
public class CollectionListResponse implements Serializable {
    @SerializedName("data")
    private ArrayList<CatgoryListData> catgoryListData;
    @SerializedName("msg")
    private String msg;
    @SerializedName("success")
    private int success;

    public ArrayList<CatgoryListData> getCatgoryListData() {
        return catgoryListData;
    }

    public void setCatgoryListData(ArrayList<CatgoryListData> data) {
        this.catgoryListData = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

}
