package com.planetshare.fragments.vidimgcollection;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.planetshare.activity.R;
import com.planetshare.adapters.collection.ImgVidCollecAdapter;
import com.planetshare.localstorage.SessionManager;
import com.planetshare.models.collection.getusercollection.UserCollectionResponse;
import com.planetshare.models.collection.getusercollection.UserData;
import com.planetshare.models.collection.usercollection.Data;
import com.planetshare.models.collection.usercollection.ImageItemInfo;
import com.planetshare.retrofit.ApiClient;
import com.planetshare.retrofit.CollectionApiInterface;
import com.planetshare.utils.Constants;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VideoCollectionFragment extends Fragment {

    Unbinder unbinder;
    @BindView(R.id.rvImageCollec)
    RecyclerView rvImageCollec;
    @BindView(R.id.pbHome)
    ProgressBar pbHome;

    private String token, collection_type = "video";
    ImgVidCollecAdapter imgVidCollecAdapter;
    private SessionManager sessionManager;
    private int userId = 0;

    ArrayList<UserData> userCollectionListData = new ArrayList<>();
    ArrayList<Data> dataArrayList = new ArrayList<>();
    ArrayList<ImageItemInfo> itemInfoArrayList = new ArrayList<>();
    private ArrayList<UserData> data = new ArrayList<>();


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_image_collection, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        initilizeView();
        sessionManager = new SessionManager(getContext());
        token = sessionManager.getToken();
        userId = sessionManager.getUserId();

        getImageCollectionData();
        return rootView;
    }

    private void initilizeView() {
        userCollectionListData = new ArrayList<>();
        dataArrayList = new ArrayList<>();
        itemInfoArrayList = new ArrayList<>();
        data = new ArrayList<>();


    }


    private void getImageCollectionData() {
        pbHome.setVisibility(View.VISIBLE);

        CollectionApiInterface apiInterface = ApiClient.createService(CollectionApiInterface.class, getContext());
        Call<UserCollectionResponse> collectionResponseCall = apiInterface.getUserCollectionResponse(userId, collection_type, token, Constants.TAG);
        collectionResponseCall.enqueue(new Callback<UserCollectionResponse>() {
            @Override
            public void onResponse(Call<UserCollectionResponse> call, Response<UserCollectionResponse> response) {
                pbHome.setVisibility(View.GONE);

                if (response.body() != null) {
                    if (response.body().getSuccess() == 1) {
                        userCollectionListData.clear();

                        if (response.body().getData() != null && response.body().getData().size() > 0) {
                            userCollectionListData.addAll(response.body().getData());
                            setupAdapter();
                        } else {
//                        Toast.makeText(getContext(), "list is empty", Toast.LENGTH_SHORT).show();
                        }
                    }
                } else {
                    Toast.makeText(getContext(), Constants.SOMETHING_WENT_WRONG, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<UserCollectionResponse> call, Throwable t) {
                pbHome.setVisibility(View.GONE);
                if (t instanceof IOException) {
                    Toast.makeText(getContext(), Constants.NETWORK_FAILURE, Toast.LENGTH_SHORT).show();
                    // logging probably not necessary
                } else {
                    Toast.makeText(getContext(), Constants.SOMETHING_WENT_WRONG, Toast.LENGTH_SHORT).show();
                    // todo log to some central bug tracking service
                }
            }
        });
    }
    private void setupAdapter() {
        imgVidCollecAdapter = new ImgVidCollecAdapter(getContext(), userCollectionListData, collection_type/*, data*/);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 2, LinearLayoutManager.VERTICAL, false);
        rvImageCollec.setLayoutManager(gridLayoutManager);
        rvImageCollec.setAdapter(imgVidCollecAdapter);
    }
}
