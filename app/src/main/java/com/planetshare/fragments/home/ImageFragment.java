package com.planetshare.fragments.home;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.planetshare.activity.R;
import com.planetshare.activity.home.DashBoardActivity;
import com.planetshare.adapters.home.ImageVerticalAdapter;
import com.planetshare.localstorage.SessionManager;
import com.planetshare.models.home.ImageCat;
import com.planetshare.utils.Constants;

import java.util.ArrayList;

public class ImageFragment extends Fragment {
    ArrayList<ImageCat> imageList;
    Context context;
    private RecyclerView rvMain;
    private TextView tvEmpty;
    ProgressBar pBar;
    SessionManager sessionManager;
    ImageVerticalAdapter fragmentVerticalAdapter;

    public static Fragment newInstance(DashBoardActivity dashBoardActivity, ArrayList<ImageCat> imageList) {
        ImageFragment imageFragment = new ImageFragment();
        Bundle args = new Bundle();
        args.putSerializable(Constants.KEYIMAGELIST, imageList);
        imageFragment.setArguments(args);
        return imageFragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        this.context = context;
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);
        if (getArguments() != null) {
            loadData(getArguments());
        }
        sessionManager = new SessionManager(getContext());
        rvMain = rootView.findViewById(R.id.rvMain);
        rvMain.setNestedScrollingEnabled(false);
        tvEmpty = rootView.findViewById(R.id.tvEmptyText);
        pBar = rootView.findViewById(R.id.pbar);
        sendDataAdapter();
        return rootView;

    }

    private void sendDataAdapter() {
        if (imageList != null && imageList.size() > 0) {
            tvEmpty.setVisibility(View.GONE);
            rvMain.setVisibility(View.VISIBLE);
            fragmentVerticalAdapter = new ImageVerticalAdapter(getContext(), imageList);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
            rvMain.setLayoutManager(linearLayoutManager);
            rvMain.setItemAnimator(new DefaultItemAnimator());
            rvMain.setAdapter(fragmentVerticalAdapter);
            linearLayoutManager.setSmoothScrollbarEnabled(true);
        } else {
            tvEmpty.setVisibility(View.VISIBLE);
        }
    }

    private void loadData(Bundle arguments) {
        imageList = (ArrayList<ImageCat>) arguments.getSerializable(Constants.KEYIMAGELIST);

    }
}
