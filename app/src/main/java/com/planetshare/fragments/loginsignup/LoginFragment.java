package com.planetshare.fragments.loginsignup;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.planetshare.activity.R;
import com.planetshare.activity.home.DashBoardActivity;
import com.planetshare.localstorage.SessionManager;
import com.planetshare.models.login.LoginResponse;
import com.planetshare.retrofit.ApiClient;
import com.planetshare.retrofit.LoginApiInterface;
import com.planetshare.utils.Constants;
import com.planetshare.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginFragment extends Fragment implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {
    Unbinder unbinder;
    @BindView(R.id.tilEmail)
    TextInputLayout tilEmail;
    @BindView(R.id.tilPassword)
    TextInputLayout tilPassword;
    @BindView(R.id.edtEmail)
    TextInputEditText edtEmail;
    @BindView(R.id.edtPassword)
    TextInputEditText edtPassword;
    @BindView(R.id.btnLogin)
    Button btnLogin;
    @BindView(R.id.cvFacebook)
    CardView cvFacebook;
    @BindView(R.id.cvGoogle)
    CardView cvGoogle;
    @BindView(R.id.pBar)
    ProgressBar pBar;
    Context context;
    SessionManager sessionManager;
    private String authProvider, userName, socialEmail, authProviderId, tag = "mobile";
    private CallbackManager callbackManager;
    private GoogleApiClient mGoogleApiClient;
    private static final int RC_SIGN_IN = 007;
    private FirebaseAuth mAuth;

    @Override
    public void onAttach(@NonNull Context mcontext) {
        super.onAttach(mcontext);
        context = mcontext;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        unbinder = ButterKnife.bind(this, view);
        sessionManager = new SessionManager(context);
        FirebaseApp.initializeApp(context);
        FacebookSdk.sdkInitialize(FacebookSdk.getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        mAuth = FirebaseAuth.getInstance();
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail().build();
        mGoogleApiClient = new GoogleApiClient.Builder(context)
                .enableAutoManage(getActivity(), this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        clickListner();
        return view;
    }


    @Override
    public void onStart() {
        super.onStart();
        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
        if (opr.isDone()) {
            GoogleSignInResult result = opr.get();

        } else {

            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(GoogleSignInResult googleSignInResult) {
                    if (googleSignInResult.isSuccess()) {
                        //Do write if hit api
                    }
                }
            });
        }
    }

    private void clickListner() {
        btnLogin.setOnClickListener(this);
        cvFacebook.setOnClickListener(this);
        cvGoogle.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cvFacebook:
                if (Utils.checkInternetConnection(context)) {
                    facebookLogin();
                } else {
                    Toast.makeText(context, "Internet Connection not available!", Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.cvGoogle:
                if (Utils.checkInternetConnection(context)) {
                    googleSignIn();
                } else {
                    Toast.makeText(context, "Internet Connection not available!", Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.btnLogin:
//                startActivity(new Intent(context, DashBoardActivity.class));
                if (Utils.checkInternetConnection(context)) {
                    /*   email*/
                    if (edtEmail.getText().toString().trim().isEmpty()) {
                        tilEmail.setError("Please enter email");
                        return;
                    }
                    tilEmail.setError(null);
                    tilEmail.setErrorEnabled(false);
                    if (!Patterns.EMAIL_ADDRESS.matcher(edtEmail.getText().toString().trim()).matches()) {
                        tilEmail.setError("Please enter valid email");
                        return;
                    }
                    tilEmail.setError(null);
                    tilEmail.setErrorEnabled(false);
                    /*password*/
                    if (edtPassword.getText().toString().trim().isEmpty()) {
                        tilPassword.setError("Please enter password");
                        return;
                    }
                    tilPassword.setError(null);
                    tilPassword.setErrorEnabled(false);
                    getLogin(edtEmail.getText().toString().trim(), edtPassword.getText().toString().trim());

                } else {
                    Toast.makeText(context, "Internet Connection not available!", Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                break;
        }
    }

    private void googleSignIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void facebookLogin() {
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email"));
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                authProvider = "facebook";
                authProviderId = loginResult.getAccessToken().getUserId();
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                try {
                                    String id = object.getString("id");
                                    String image_url = "http://graph.facebook.com/" + id + "/picture?type=large";
                                    String last_name = object.getString("last_name");
                                    String gender = object.getString("gender");
                                    String birthday = object.getString("birthday");
                                    //String email = response.getJSONObject().getString("email");
                                    if (object.getString("first_name") != null)
                                        userName = object.getString("first_name");
                                    if (object.getString("email") != null)
                                        socialEmail = object.getString("email");
                                    else
                                        socialEmail = "";
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                getFacebookResponse();
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,email,first_name");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                Toast.makeText(context, "Facebook Login cancelled", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(context, "Facebook Login Error", Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void handleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            GoogleSignInAccount acct = result.getSignInAccount();
            authProvider = "Google";
            authProviderId = acct.getId();
            String personName = acct.getDisplayName();
            if (acct.getPhotoUrl() != null) {
                String googleImageUrl = acct.getPhotoUrl().toString();
            }
            userName = acct.getDisplayName();
            socialEmail = acct.getEmail();
        } else {
            Toast.makeText(context, "Could not sign in via google", Toast.LENGTH_SHORT).show();
        }
    }

    private void getFacebookResponse() {
    }

    private void getLogin(String edtEmail, String edtPassword) {
        pBar.setVisibility(View.VISIBLE);
        LoginApiInterface loginApiInterface = ApiClient.createServiceNew(LoginApiInterface.class,context);
        Call<LoginResponse> loginResponse = loginApiInterface.getLoginResponse(edtEmail, edtPassword, tag);
        loginResponse.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                pBar.setVisibility(View.GONE);
                if (response.body() != null) {
                    if (response.body().getSuccess() == 0) {
                        String msg = response.body().getMsg();
                        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                    } else if (response.body().getSuccess() == 1) {
                        sessionManager.saveToken(response.body().getToken());
                        sessionManager.saveAccountId(response.body().getAccountId());
                        sessionManager.saveUserName(response.body().getName());
                        sessionManager.saveEmailId(response.body().getEmail());
                        sessionManager.saveUserId((response.body().getUserId()), true);
                        sessionManager.saveVendorId(response.body().getVendorId());
                        sessionManager.saveSellerId(response.body().getSellerId());
                        Intent intent = new Intent(context, DashBoardActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        context.startActivity(intent);

                        Toast.makeText(context, "Login Successfully", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, Constants.SOMETHING_WENT_WRONG, Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                pBar.setVisibility(View.GONE);
                Toast.makeText(context, Constants.SOMETHING_WENT_WRONG, Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }


    @Override
    public void onDestroyView() {
        if (unbinder != null) {
            unbinder.unbind();
        }
        super.onDestroyView();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
