package com.planetshare.gestures;

import android.view.MotionEvent;

public interface IGestureListener {
    void onTap();

    void onHorizontalScroll(MotionEvent event, float delta);

    void onSwipeRight();

    void onSwipeLeft();

    void onSwipeBottom();

    void onSwipeTop();

    void brightness(int value);

    void volume(int value);

    void onScrollEnd();

}
