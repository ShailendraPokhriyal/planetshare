package com.planetshare.activity.collection;

import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.planetshare.activity.R;
import com.planetshare.localstorage.SessionManager;
import com.planetshare.models.collection.collectionlist.CatgoryListData;
import com.planetshare.models.collection.collectionlist.CollectionListResponse;
import com.planetshare.retrofit.ApiClient;
import com.planetshare.retrofit.CollectionApiInterface;
import com.planetshare.utils.Constants;
import com.planetshare.utils.Utils;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CollectionListActivity extends AppCompatActivity {
    private String collectionType, token;
    private int collectionId, userId = 0;
    SessionManager sessionManager;
    private boolean isNetworkConnected;
    private ArrayList<CatgoryListData> catgoryListData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collection);
        ButterKnife.bind(this);
        sessionManager = new SessionManager(this);
        if (sessionManager.getToken() != null) {
            token = sessionManager.getToken();
        }
        userId = sessionManager.getUserId();
        /*InitializeView*/
        initializeView();
        getCollectionData(collectionType, collectionId);
    }

    private void initializeView() {
        isNetworkConnected = Utils.checkInternetConnection(this);
        if (!isNetworkConnected) {
            Utils.restartDialogue(this, Constants.NETWORK_ERROR, true);
        }
    }

    private void getCollectionData(String collectionType, int collectionId) {
        CollectionApiInterface collectionApiInterface = ApiClient.createService(CollectionApiInterface.class, this);
        Call<CollectionListResponse> collectionListResponse = collectionApiInterface.getCollectionData(userId, collectionType, collectionId, token, Constants.TAG);
        collectionListResponse.enqueue(new Callback<CollectionListResponse>() {
            @Override
            public void onResponse(Call<CollectionListResponse> call, Response<CollectionListResponse> response) {
                if (response.body() != null) {
                    if (response.body().getSuccess() == 1) {
                        if (response.body().getCatgoryListData() != null && response.body().getCatgoryListData().size() > 0) {

                        } else {
                            Toast.makeText(CollectionListActivity.this, "No Data Found", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(CollectionListActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<CollectionListResponse> call, Throwable t) {
                if (t instanceof IOException) {
                    Toast.makeText(CollectionListActivity.this, Constants.NETWORK_FAILURE, Toast.LENGTH_SHORT).show();
                    // logging probably not necessary
                } else {
                    Toast.makeText(CollectionListActivity.this, Constants.SOMETHING_WENT_WRONG, Toast.LENGTH_SHORT).show();
                    // todo log to some central bug tracking service
                }
            }
        });
    }
}
