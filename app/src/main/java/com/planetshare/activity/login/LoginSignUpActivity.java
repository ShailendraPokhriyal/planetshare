package com.planetshare.activity.login;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.planetshare.activity.R;
import com.planetshare.adapters.viewpagerAdpater.ViewPagerAdapter;
import com.planetshare.fragments.loginsignup.LoginFragment;
import com.planetshare.fragments.loginsignup.SignUpFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginSignUpActivity extends AppCompatActivity implements TabLayout.BaseOnTabSelectedListener {
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.tabLayout)
    TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_sign_up);
        ButterKnife.bind(this);
        setTabs();
    }

    private void setTabs() {
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.addFragment(new LoginFragment(), "Log in");
        viewPagerAdapter.addFragment(new SignUpFragment(), "Sign up");
        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.addOnTabSelectedListener(this);
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());

    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
