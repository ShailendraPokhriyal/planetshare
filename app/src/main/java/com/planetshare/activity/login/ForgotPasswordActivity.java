package com.planetshare.activity.login;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.planetshare.activity.R;

public class ForgotPasswordActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
    }
}
