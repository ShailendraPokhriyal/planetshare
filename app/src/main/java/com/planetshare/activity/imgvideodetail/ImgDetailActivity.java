package com.planetshare.activity.imgvideodetail;

import android.app.Dialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.bumptech.glide.Glide;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.planetshare.activity.R;
import com.planetshare.activity.home.DashBoardActivity;
import com.planetshare.adapters.imgvideodetail.ImgAdapter;
import com.planetshare.adapters.imgvideodetail.PlanAdapter;
import com.planetshare.adapters.imgvideodetail.TagsAdapter;
import com.planetshare.adapters.imgvideodetail.UserCollectionListAdpater;
import com.planetshare.localstorage.SessionManager;
import com.planetshare.models.collection.createcollection.CreateCollectionResponse;
import com.planetshare.models.collection.getusercollection.UserCollectionResponse;
import com.planetshare.models.collection.getusercollection.UserData;
import com.planetshare.models.collection.insertcollection.InsertCollectionResposne;
import com.planetshare.models.imagevideodetails.imgdetail.ImageResponse;
import com.planetshare.models.imagevideodetails.imgdetail.SimilarDataImage;
import com.planetshare.models.plans.PlanData;
import com.planetshare.models.plans.PlansResponse;
import com.planetshare.retrofit.ApiClient;
import com.planetshare.retrofit.CollectionApiInterface;
import com.planetshare.retrofit.ImgVidInterface;
import com.planetshare.retrofit.PlanApiInterface;
import com.planetshare.utils.Constants;
import com.planetshare.utils.Utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ImgDetailActivity extends AppCompatActivity implements View.OnClickListener, UserCollectionListAdpater.ClickItemCallBack, PlanAdapter.PlanItemClick {
    @BindView(R.id.rvImgVidDetails)
    RecyclerView rvImgVidDetails;
    @BindView(R.id.rvTags)
    RecyclerView rvTags;
    @BindView(R.id.pbarImgVidDetails)
    ProgressBar pbarImgDetails;
    @BindView(R.id.collapsingToolbar)
    CollapsingToolbarLayout collapsingToolbar;
    @BindView(R.id.appBarLayout)
    AppBarLayout appBarLayout;
    @BindView(R.id.ivThumb)
    ImageView ivThumb;
    @BindView(R.id.ivDownload)
    ImageView ivDownload;
    @BindView(R.id.ivShare)
    ImageView ivShare;
    @BindView(R.id.ivImgInfo)
    ImageView ivImgInfo;
    @BindView(R.id.ivCross)
    ImageView ivCross;
    @BindView(R.id.ivAddToCollection)
    ImageView ivAddToCollection;
    @BindView(R.id.tvName)
    TextView tvName;
    String titleName, imageCatId, imageId, catName, thumbUrl, keywords, itemId, collectionName, cartStatus;
    private int userId = 0, collectionId, planStatus;

    SessionManager sessionManager;
    private boolean isNetworkConnected;
    private String token;
    private String totalViews, totalSize, totalDownloads, totalLikes;
    private RecyclerView rvBottomPlans;
    Button btnSaveCollection;
    ProgressBar pbarCollection;
    RecyclerView rvUsersList;
    TextView tvView, tvSize, tvLikes, tvDownloads, tvCamera;
    Dialog dialog;
    /*ArrayList*/
    private ArrayList<PlanData> planData;
    private ArrayList<UserData> userCollectionListData;
    private ArrayList<SimilarDataImage> similarData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_img_detail);
        ButterKnife.bind(this);
        sessionManager = new SessionManager(this);
        token = sessionManager.getToken();
        userId = sessionManager.getUserId();
        Log.d("Token", token);
        Log.d("UserId", String.valueOf(userId));
        if (getIntent().getStringExtra("thumbUrl") != null) {
            thumbUrl = getIntent().getStringExtra("thumbUrl");
        }
        if (getIntent().getStringExtra("imageId") != null) {
            imageId = getIntent().getStringExtra("imageId");
            itemId = imageId;
        }
        if (getIntent().getStringExtra("imageCatId") != null) {
            imageCatId = getIntent().getStringExtra("imageCatId");
        }
        titleName = getIntent().getStringExtra("titleName");
        catName = getIntent().getStringExtra("catName");
        if (getSupportActionBar() != null) {
            ActionBar actionBar = getSupportActionBar();
            actionBar.setDisplayHomeAsUpEnabled(false);
        }
        initializeView();
        isNetworkConnected = Utils.checkInternetConnection(this);
        if (!isNetworkConnected) {
            Utils.restartDialogue(this, Constants.NETWORK_ERROR, true);
        }
        getImgDetails(imageId);
        titleName = getIntent().getStringExtra("name");
        collapsingToolbar.setTitle(titleName);
    }


    private void initializeView() {
        /*BeforePlayer*/
        userCollectionListData = new ArrayList<>();
        planData = new ArrayList<>();
        similarData = new ArrayList<>();
        rvImgVidDetails.setHasFixedSize(true);
        clickListner();
    }


    private void clickListner() {
        ivCross.setOnClickListener(this);
        ivShare.setOnClickListener(this);
        ivAddToCollection.setOnClickListener(this);
        ivImgInfo.setOnClickListener(this);
        ivDownload.setOnClickListener(this);
    }

    private void getImgDetails(String imageId) {
        pbarImgDetails.setVisibility(View.VISIBLE);
        ImgVidInterface imgVidInterface = ApiClient.createService(ImgVidInterface.class, this);
        Call<ImageResponse> imgVidResponse = imgVidInterface.getImgResponse(userId, imageId, Constants.TAG);
        imgVidResponse.enqueue(new Callback<ImageResponse>() {
            @Override
            public void onResponse(Call<ImageResponse> call, Response<ImageResponse> response) {
                pbarImgDetails.setVisibility(View.GONE);
                if (response.body() != null) {
                    if (response.body().getSuccess() == 1) {
                        similarData.clear();
                        if (response.body().getSimilarData() != null && response.body().getSimilarData().size() > 0) {
                            similarData.addAll(response.body().getSimilarData());
                            rvImgVidDetails.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
                            ImgAdapter imgVidAdapter = new ImgAdapter(ImgDetailActivity.this, similarData, ImgDetailActivity.this.catName);
                            rvImgVidDetails.setAdapter(imgVidAdapter);
                            /* SpacesItemDecoration decoration = new SpacesItemDecoration(40);
                            rvImgVidDetails.addItemDecoration(decoration);*/
                        }
                        if (response.body().getData() != null) {
                            Glide.with(getApplicationContext())
                                    .load(response.body().getData().getLargeThumb())
                                    .into(ivThumb);
                            tvName.setText(response.body().getData().getTitle());
                            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ImgDetailActivity.this, LinearLayoutManager.HORIZONTAL, false);
                            rvTags.setLayoutManager(linearLayoutManager);
                            List<String> keyWordList = Arrays.asList(response.body().getData().getKeywords().split(","));
                            TagsAdapter tagsAdapter = new TagsAdapter(ImgDetailActivity.this, keyWordList);
                            rvTags.setAdapter(tagsAdapter);
                        }
                        if (response.body().getCartStatus() == 0) {
                            ivAddToCollection.setImageResource(R.drawable.addtocart);
                            ivAddToCollection.setClickable(true);
                        } else {
                            ivAddToCollection.setImageResource(R.drawable.addtocartsaved);
                            ivAddToCollection.setClickable(false);
                        }
                        planStatus = response.body().getPackStatusCheck();
                    }
                } else {
                    Toast.makeText(ImgDetailActivity.this, "No data found", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ImageResponse> call, Throwable t) {
                pbarImgDetails.setVisibility(View.GONE);
                if (t instanceof IOException) {
                    Toast.makeText(ImgDetailActivity.this, Constants.NETWORK_FAILURE, Toast.LENGTH_SHORT).show();
                    // logging probably not necessary
                } else {
                    Toast.makeText(ImgDetailActivity.this, Constants.SOMETHING_WENT_WRONG, Toast.LENGTH_SHORT).show();
                    // todo log to some central bug tracking service
                }

            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivCross:
                onBackPressed();
                break;
            case R.id.ivAddToCollection:
                addToCollection();
                break;
            case R.id.ivShare:
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, "This is my text to send.");
                sendIntent.setType("text/plain");
                Intent shareIntent = Intent.createChooser(sendIntent, null);
                startActivity(shareIntent);
                break;
            case R.id.ivImgInfo:
                showImgInformation();
                break;
            case R.id.ivDownload:
                getPlans();
                break;
            default:
                break;
        }
    }

    private void addToCollection() {
        dialog = new Dialog(this, R.style.FullScreenDialogStyle);
        int width = ViewGroup.LayoutParams.MATCH_PARENT;
        int height = ViewGroup.LayoutParams.MATCH_PARENT;
        dialog.getWindow().setLayout(width, height);
        dialog.setContentView(R.layout.collection_dialog_layout);
        EditText edtCollection = dialog.findViewById(R.id.edtCollection);
        pbarCollection = dialog.findViewById(R.id.pbarCollection);
        rvUsersList = dialog.findViewById(R.id.rvUsersList);
        btnSaveCollection = dialog.findViewById(R.id.btnSaveCollection);
        /*UserListing With Name*/
        getUserList();
        /*Create a UserCollectionWith Name*/
        btnSaveCollection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createCollection(edtCollection.getText().toString().trim());
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    /*ImgInformation*/
    private void showImgInformation() {
        final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(ImgDetailActivity.this, R.style.SheetDialog);
        View sheetView = LayoutInflater.from(ImgDetailActivity.this).inflate(R.layout.layout_bottom_imginformation, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.show();
        tvView = sheetView.findViewById(R.id.tvView);
        tvSize = sheetView.findViewById(R.id.tvSize);
        tvLikes = sheetView.findViewById(R.id.tvLikes);
        tvDownloads = sheetView.findViewById(R.id.tvDownloads);
        tvCamera = sheetView.findViewById(R.id.tvCamera);
        tvCamera.setText(getDeviceName());

    }

    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        }
        return capitalize(manufacturer) + " " + model;
    }

    private static String capitalize(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        char[] arr = str.toCharArray();
        boolean capitalizeNext = true;
        String phrase = "";
        for (char c : arr) {
            if (capitalizeNext && Character.isLetter(c)) {
                phrase += Character.toUpperCase(c);
                capitalizeNext = false;
                continue;
            } else if (Character.isWhitespace(c)) {
                capitalizeNext = true;
            }
            phrase += c;
        }
        return phrase;
    }

    private void getPlans() {
        PlanApiInterface planApiInterface = ApiClient.createService(PlanApiInterface.class, this);
        Call<PlansResponse> plansResponse = planApiInterface.getPlanResponse(userId, catName, token, Constants.TAG);
        plansResponse.enqueue(new Callback<PlansResponse>() {
            @Override
            public void onResponse(Call<PlansResponse> call, Response<PlansResponse> response) {
                if (response.body() != null) {
                    if (response.body().getSuccess() == 1) {
                        planData.clear();
                        if (response.body().getPlanData() != null && response.body().getPlanData().size() > 0) {
                            planData.addAll(response.body().getPlanData());
                            if (planStatus == 0) {
                                showPlans(planData);
                            } else {
                                Toast.makeText(ImgDetailActivity.this, "Download Now", Toast.LENGTH_SHORT).show();
                            }
                        }
                    } else {
                        Toast.makeText(ImgDetailActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(ImgDetailActivity.this, "No Data Found", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<PlansResponse> call, Throwable t) {
                if (t instanceof IOException) {
                    Toast.makeText(ImgDetailActivity.this, Constants.NETWORK_FAILURE, Toast.LENGTH_SHORT).show();
                    // logging probably not necessary
                } else {
                    Toast.makeText(ImgDetailActivity.this, Constants.SOMETHING_WENT_WRONG, Toast.LENGTH_SHORT).show();
                    // todo log to some central bug tracking service
                }
            }
        });
    }

    /*Plans*/
    private void showPlans(ArrayList<PlanData> planData) {
        final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(ImgDetailActivity.this, R.style.SheetDialog);
        View sheetView = LayoutInflater.from(ImgDetailActivity.this).inflate(R.layout.layout_bottom_plans, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.show();
        rvBottomPlans = sheetView.findViewById(R.id.rvBottomPlans);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(ImgDetailActivity.this, 2);
        rvBottomPlans.setLayoutManager(gridLayoutManager);
        PlanAdapter planAdapter = new PlanAdapter(ImgDetailActivity.this, ImgDetailActivity.this, planData);
        rvBottomPlans.setAdapter(planAdapter);
    }

    /*Create a Collection */
    private void createCollection(String edtCollection) {
        pbarCollection.setVisibility(View.VISIBLE);
        collectionName = edtCollection;
        CollectionApiInterface createCollectionApiInterface = ApiClient.createService(CollectionApiInterface.class, this);
        Call<CreateCollectionResponse> createCollectionResponse = createCollectionApiInterface.getCreateCollectionResponse(userId, catName, collectionName, token, Constants.TAG);
        createCollectionResponse.enqueue(new Callback<CreateCollectionResponse>() {
            @Override
            public void onResponse(Call<CreateCollectionResponse> call, Response<CreateCollectionResponse> response) {
                pbarCollection.setVisibility(View.GONE);
                if (response.body() != null) {
                    if (response.body().getSuccess() == 0) {
                        Toast.makeText(ImgDetailActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                    if (response.body().getSuccess() == 1) {
                        collectionId = response.body().getCollectionId();
                        insertItem(collectionId);
                    }
                    if (response.body().getSuccess() == 2) {
                        Toast.makeText(ImgDetailActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(ImgDetailActivity.this, Constants.NETWORK_ERROR, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CreateCollectionResponse> call, Throwable t) {
                pbarCollection.setVisibility(View.GONE);
                if (t instanceof IOException) {
                    Toast.makeText(ImgDetailActivity.this, Constants.NETWORK_FAILURE, Toast.LENGTH_SHORT).show();
                    // logging probably not necessary
                } else {
                    Toast.makeText(ImgDetailActivity.this, Constants.SOMETHING_WENT_WRONG, Toast.LENGTH_SHORT).show();
                    // todo log to some central bug tracking service
                }
            }
        });
    }

    /*UserList ShowIn in Dialog*/
    private void getUserList() {
        pbarCollection.setVisibility(View.VISIBLE);
        CollectionApiInterface collectionApiInterface = ApiClient.createService(CollectionApiInterface.class, this);
        Call<UserCollectionResponse> userCollectionResponse = collectionApiInterface.getUserCollectionResponse(userId, catName, token, Constants.TAG);
        userCollectionResponse.enqueue(new Callback<UserCollectionResponse>() {
            @Override
            public void onResponse(Call<UserCollectionResponse> call, Response<UserCollectionResponse> response) {
                pbarCollection.setVisibility(View.GONE);
                if (response.body() != null) {
                    if (response.body().getSuccess() == 1) {
                        userCollectionListData.clear();
                        if (response.body().getData() != null && response.body().getData().size() > 0) {
                            userCollectionListData.addAll(response.body().getData());
                            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ImgDetailActivity.this, LinearLayoutManager.HORIZONTAL, false);
                            rvUsersList.setLayoutManager(linearLayoutManager);
                            UserCollectionListAdpater userCollectionListAdpater = new UserCollectionListAdpater(ImgDetailActivity.this, ImgDetailActivity.this, userCollectionListData, catName);
                            rvUsersList.setAdapter(userCollectionListAdpater);
                            userCollectionListAdpater.notifyDataSetChanged();
                        }
                    } else {
                        Toast.makeText(ImgDetailActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<UserCollectionResponse> call, Throwable t) {
                pbarCollection.setVisibility(View.GONE);
                if (t instanceof IOException) {
                    Toast.makeText(ImgDetailActivity.this, Constants.NETWORK_FAILURE, Toast.LENGTH_SHORT).show();
                    // logging probably not necessary
                } else {
                    Toast.makeText(ImgDetailActivity.this, Constants.SOMETHING_WENT_WRONG, Toast.LENGTH_SHORT).show();
                    // todo log to some central bug tracking service
                }
            }
        });
    }

    /*InsertItemInCollection*/
    private void insertItem(int collectionId) {
        pbarCollection.setVisibility(View.VISIBLE);
        CollectionApiInterface collectionApiInterface = ApiClient.createService(CollectionApiInterface.class, this);
        Call<InsertCollectionResposne> insertCollectionResposne = collectionApiInterface.getInsertResponse(userId, itemId, catName, collectionId, token, Constants.TAG);
        insertCollectionResposne.enqueue(new Callback<InsertCollectionResposne>() {
            @Override
            public void onResponse(Call<InsertCollectionResposne> call, Response<InsertCollectionResposne> response) {
                pbarCollection.setVisibility(View.GONE);
                if (response.body() != null) {
                    if (response.body().getSuccess() == 0) {
                        Toast.makeText(ImgDetailActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                    if (response.body().getSuccess() == 1) {
                        Toast.makeText(ImgDetailActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                        getImgDetails(itemId);
                    }
                    if (response.body().getSuccess() == 2) {
                        Toast.makeText(ImgDetailActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(ImgDetailActivity.this, Constants.NETWORK_ERROR, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<InsertCollectionResposne> call, Throwable t) {
                pbarCollection.setVisibility(View.GONE);
                if (t instanceof IOException) {
                    Toast.makeText(ImgDetailActivity.this, Constants.NETWORK_FAILURE, Toast.LENGTH_SHORT).show();
                    // logging probably not necessary
                } else {
                    Toast.makeText(ImgDetailActivity.this, Constants.SOMETHING_WENT_WRONG, Toast.LENGTH_SHORT).show();
                    // todo log to some central bug tracking service
                }
            }
        });

    }

    /*Related CallBack for Reloading Data*/
    public void relatedCallBackFromAdpater(String imageId) {
        getImgDetails(imageId);
        itemId = imageId;
    }

    /*UserDataListAdpaterCallBack*/
    @Override
    public void onItemClickCallBack(int collectionId) {
        insertItem(collectionId);
        dialog.dismiss();

    }

    /*PlansClick*/
    @Override
    public void PlanItemClick(PlanData planData) {

    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, DashBoardActivity.class));
        overridePendingTransition(0,
                R.anim.animbackpress
        );
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


}

