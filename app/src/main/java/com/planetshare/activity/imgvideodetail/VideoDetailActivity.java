package com.planetshare.activity.imgvideodetail;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.PictureInPictureParams;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.interpolator.view.animation.FastOutSlowInInterpolator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackPreparer;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.BehindLiveWindowException;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.source.dash.DashMediaSource;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.source.smoothstreaming.SsMediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.MappingTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.PlayerControlView;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.planetshare.activity.R;
import com.planetshare.activity.home.DashBoardActivity;
import com.planetshare.adapters.imgvideodetail.PlanAdapter;
import com.planetshare.adapters.imgvideodetail.TagsAdapter;
import com.planetshare.adapters.imgvideodetail.UserCollectionListAdpater;
import com.planetshare.adapters.imgvideodetail.VidAdapter;
import com.planetshare.gestures.AppPreferences;
import com.planetshare.gestures.AudioReactor;
import com.planetshare.gestures.GestureListener;
import com.planetshare.gestures.PlayIconDrawable;
import com.planetshare.localstorage.SessionManager;
import com.planetshare.models.collection.createcollection.CreateCollectionResponse;
import com.planetshare.models.collection.getusercollection.UserCollectionResponse;
import com.planetshare.models.collection.getusercollection.UserData;
import com.planetshare.models.collection.insertcollection.InsertCollectionResposne;
import com.planetshare.models.imagevideodetails.viddetail.SimilarDataVid;
import com.planetshare.models.imagevideodetails.viddetail.VidDetailResponse;
import com.planetshare.models.plans.PlanData;
import com.planetshare.models.plans.PlansResponse;
import com.planetshare.retrofit.ApiClient;
import com.planetshare.retrofit.CollectionApiInterface;
import com.planetshare.retrofit.ImgVidInterface;
import com.planetshare.retrofit.PlanApiInterface;
import com.planetshare.utils.Constants;
import com.planetshare.utils.Utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.planetshare.gestures.AnimationUtils.Type.SCALE_AND_ALPHA;
import static com.planetshare.gestures.AnimationUtils.animateView;

public class VideoDetailActivity extends AppCompatActivity implements View.OnClickListener, UserCollectionListAdpater.ClickItemCallBack, PlaybackPreparer, PlayerControlView.VisibilityListener, PlanAdapter.PlanItemClick {
    @BindView(R.id.rvVidDetails)
    RecyclerView rvVidDetails;
    @BindView(R.id.rvTags)
    RecyclerView rvTags;
    @BindView(R.id.pbarVidDetails)
    ProgressBar pbarImgDetails;
    @BindView(R.id.pb_loading)
    ProgressBar pb_loading;
    @BindView(R.id.collapsingToolbar)
    CollapsingToolbarLayout collapsingToolbar;
    @BindView(R.id.appBarLayout)
    AppBarLayout appBarLayout;
    @BindView(R.id.ivDownload)
    ImageView ivDownload;
    @BindView(R.id.ivShare)
    ImageView ivShare;
    @BindView(R.id.ivImgInfo)
    ImageView ivImgInfo;
    @BindView(R.id.ivCross)
    ImageView ivCross;
    @BindView(R.id.ivAddToCollection)
    ImageView ivAddToCollection;
    @BindView(R.id.tvName)
    TextView tvName;
    /*Exoplayer*/
    @BindView(R.id.setting)
    ImageView settingview;
    @BindView(R.id.brightnessImageView)
    ImageView brIV;
    @BindView(R.id.volumeImageView)
    ImageView vIV;
    @BindView(R.id.brightnessProgressBar)
    ProgressBar brPG;
    @BindView(R.id.volumeProgressBar)
    ProgressBar vPG;
    @BindView(R.id.pause_play_button)
    ImageView playPauseImageView;
    @BindView(R.id.brightnessRelativeLayout)
    RelativeLayout brView;
    @BindView(R.id.volumeRelativeLayout)
    RelativeLayout volumeView;
    @BindView(R.id.player_view)
    PlayerView playerView;
    @BindView(R.id.exo_controller)
    PlayerControlView controlView;
    PlayIconDrawable play;
    /*EndExoplayer*/
    String titleName, imageCatId, videoId, catName, thumbUrl, keywords, itemId, collectionName, CartStatus, SubscriptionStatus, VideoUrl;
    SessionManager sessionManager;
    private int userId = 0, collectionId, planStatus;

    private boolean isNetworkConnected;
    private String token;

    private String totalViews, totalSize, totalDownloads, totalLikes;

    ProgressBar pbarCollection;
    private RecyclerView rvBottomPlans;
    RecyclerView rvUsersList;
    Button btnSaveCollection;
    EditText edtCollection;
    TextView tvView, tvSize, tvLikes, tvDownloads, tvCamera;
    Dialog dialog;
    /*ArrayList*/
    private ArrayList<PlanData> planData;
    private ArrayList<SimilarDataVid> similarData;
    private ArrayList<UserData> userCollectionListData;
    /*Player*/
    private SimpleExoPlayer simpleExoPlayer;
    private boolean playWhenReady = true;
    private int currentWindow = 0;
    private long playbackPosition = 0;
    private ImageView mFullScreenIcon;
    private FrameLayout mFullScreenButton;
    private boolean mExoPlayerFullscreen = false;
    Boolean Rotation = false;
    Boolean RotationFull = false;
    private LinearLayout llPlayerLayout;
    /*FloatingPlayer*/
    PictureInPictureParams.Builder mPictureInPictureParamsBuilder = null;
    static boolean active = false;
    MediaSource mediaSource;
    public final String extension = null;
    private int resumeWindow;
    private long resumePosition;
    private boolean inErrorState;
    private int playminute, lastduration;
    private DefaultTrackSelector trackSelector;
    AudioReactor audioReactor;
    int maxVolume = 0;
    private TrackGroupArray lastSeenTrackGroupArray;
    private boolean shouldAutoPlay;
    public static final int DEFAULT_CONTROLS_DURATION = 300; // 300 millis
    Activity activity;
    int maxGestureLength;
    private static final boolean DEBUG = Boolean.parseBoolean("true");
    protected static String TAG;
    private final float MAX_GESTURE_LENGTH = 0.75f;
    AppPreferences playerPrefrence;
    int screenWidth;
    int screenHeigth;
    boolean gestureSeek = false;
    TextView timeText, seekStatus;
    int currentIndex = 0;
    long gestureSeekPosition = 0;
    int gestureSeekIndex = 0;
    private static final DefaultBandwidthMeter BANDWIDTH_METER = new DefaultBandwidthMeter();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_detail);
        ButterKnife.bind(this);
        sessionManager = new SessionManager(this);
        token = sessionManager.getToken();
        userId = sessionManager.getUserId();
        Log.d("Token", token);
        Log.d("UserId", String.valueOf(userId));
        if (getIntent().getStringExtra("videoId") != null) {
            videoId = getIntent().getStringExtra("videoId");
            itemId = videoId;
        }
        if (getIntent().getStringExtra("imageCatId") != null) {
            imageCatId = getIntent().getStringExtra("imageCatId");
        }
        titleName = getIntent().getStringExtra("titleName");

        catName = getIntent().getStringExtra("catName");

        if (getSupportActionBar() != null) {
            ActionBar actionBar = getSupportActionBar();
            actionBar.setDisplayHomeAsUpEnabled(false);
        }
        play = PlayIconDrawable.builder()
                .withColor(Color.WHITE)
                .withInterpolator(new FastOutSlowInInterpolator())
                .withDuration(300)
                .withInitialState(PlayIconDrawable.IconState.PAUSE)
                .withAnimatorListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        Log.d("Animation", "animationFinished");
                    }
                })
                .withStateListener(new PlayIconDrawable.StateListener() {
                    @Override
                    public void onStateChanged(PlayIconDrawable.IconState state) {
                        Log.d("IconState", "onStateChanged: " + state);

                    }
                })
                .into(playPauseImageView);
        shouldAutoPlay = true;
        clearResumePosition();
        initializeView();
        isNetworkConnected = Utils.checkInternetConnection(this);
        if (!isNetworkConnected) {
            Utils.restartDialogue(this, Constants.NETWORK_ERROR, true);
        }
        getVidDetails(videoId);
    }

    private void initializeView() {
        /*BeforePlayer*/
        activity = VideoDetailActivity.this;
        planData = new ArrayList<>();
        userCollectionListData = new ArrayList<>();
        similarData = new ArrayList<>();
        rvVidDetails.setHasFixedSize(true);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            mPictureInPictureParamsBuilder = new PictureInPictureParams.Builder();
        }
        clickListner();
    }

    private void clickListner() {
        ivCross.setOnClickListener(this);
        ivShare.setOnClickListener(this);
        ivAddToCollection.setOnClickListener(this);
        ivImgInfo.setOnClickListener(this);
        ivDownload.setOnClickListener(this);

    }

    private void getVidDetails(String videoId) {
        pbarImgDetails.setVisibility(View.VISIBLE);
        ImgVidInterface imgVidInterface = ApiClient.createService(ImgVidInterface.class, this);
        Call<VidDetailResponse> imgVidResponse = imgVidInterface.getVideoResponse(userId, videoId, Constants.TAG);
        imgVidResponse.enqueue(new Callback<VidDetailResponse>() {
            @Override
            public void onResponse(Call<VidDetailResponse> call, Response<VidDetailResponse> response) {
                pbarImgDetails.setVisibility(View.GONE);
                if (response.body() != null) {
                    if (response.body().getSuccess() == 1) {
                        similarData.clear();
                        if (response.body().getSimilarData() != null && response.body().getSimilarData().size() > 0) {
                            similarData.addAll(response.body().getSimilarData());
                            rvVidDetails.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
                            VidAdapter imgVidAdapter = new VidAdapter(VideoDetailActivity.this, similarData, VideoDetailActivity.this.catName);
                            rvVidDetails.setAdapter(imgVidAdapter);
                            /* SpacesItemDecoration decoration = new SpacesItemDecoration(40);
                            rvVidDetails.addItemDecoration(decoration);*/
                        }
                        if (response.body().getData() != null) {
                            if (response.body().getData().getDemoUrl() != null) {
                                VideoUrl = response.body().getData().getDemoUrl();
                            }
                            playminute = Integer.parseInt(response.body().getData().getVideoLength());
                            if (VideoUrl != null) {
                                settingview.setVisibility(View.GONE);
                                playerView.setVisibility(View.VISIBLE);
                                if (simpleExoPlayer != null)
                                    playVideo(VideoUrl);
                            }
                            tvName.setText(response.body().getData().getTitle());
                            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(VideoDetailActivity.this, LinearLayoutManager.HORIZONTAL, false);
                            rvTags.setLayoutManager(linearLayoutManager);
                            List<String> keyWordList = Arrays.asList(response.body().getData().getKeywords().split(","));
                            TagsAdapter tagsAdapter = new TagsAdapter(VideoDetailActivity.this, keyWordList);
                            rvTags.setAdapter(tagsAdapter);

                        }
                        if (response.body().getCartStatus() == 0) {
                            ivAddToCollection.setImageResource(R.drawable.addtocart);
                            ivAddToCollection.setClickable(true);
                        } else {
                            ivAddToCollection.setImageResource(R.drawable.addtocartsaved);
                            ivAddToCollection.setClickable(false);
                        }
                        planStatus = response.body().getPackStatusCheck();
                    }
                } else {
                    Toast.makeText(VideoDetailActivity.this, "No data found", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<VidDetailResponse> call, Throwable t) {
                pbarImgDetails.setVisibility(View.GONE);
                if (t instanceof IOException) {
                    Toast.makeText(VideoDetailActivity.this, Constants.NETWORK_FAILURE, Toast.LENGTH_SHORT).show();
                    // logging probably not necessary
                } else {
                    Toast.makeText(VideoDetailActivity.this, Constants.SOMETHING_WENT_WRONG, Toast.LENGTH_SHORT).show();
                    // todo log to some central bug tracking service
                }

            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivCross:
                onBackPressed();
                break;
            case R.id.ivAddToCollection:
                addToCollection();
                break;
            case R.id.ivShare:
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, "This is my text to send.");
                sendIntent.setType("text/plain");
                Intent shareIntent = Intent.createChooser(sendIntent, null);
                startActivity(shareIntent);
                break;
            case R.id.ivImgInfo:
                showVidInformation();
                break;
            case R.id.ivDownload:
                getPlans();
                break;
            default:
                break;
        }
    }


    private void addToCollection() {
        dialog = new Dialog(this, R.style.FullScreenDialogStyle);
        int width = ViewGroup.LayoutParams.MATCH_PARENT;
        int height = ViewGroup.LayoutParams.MATCH_PARENT;
        dialog.getWindow().setLayout(width, height);
        dialog.setContentView(R.layout.collection_dialog_layout);
        edtCollection = dialog.findViewById(R.id.edtCollection);
        pbarCollection = dialog.findViewById(R.id.pbarCollection);
        rvUsersList = dialog.findViewById(R.id.rvUsersList);
        btnSaveCollection = dialog.findViewById(R.id.btnSaveCollection);
        /*UserListing With Name*/
        getUserList();
        /*Create a UserCollectionWith Name*/
        btnSaveCollection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createCollection(edtCollection.getText().toString().trim());
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    /*ImgInformation*/
    private void showVidInformation() {
        final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(VideoDetailActivity.this, R.style.SheetDialog);
        View sheetView = LayoutInflater.from(VideoDetailActivity.this).inflate(R.layout.layout_bottom_imginformation, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.show();
        tvView = sheetView.findViewById(R.id.tvView);
        tvSize = sheetView.findViewById(R.id.tvSize);
        tvLikes = sheetView.findViewById(R.id.tvLikes);
        tvDownloads = sheetView.findViewById(R.id.tvDownloads);
        tvCamera = sheetView.findViewById(R.id.tvCamera);
        tvCamera.setText(getDeviceName());

    }

    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        }
        return capitalize(manufacturer) + " " + model;
    }

    private static String capitalize(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        char[] arr = str.toCharArray();
        boolean capitalizeNext = true;
        String phrase = "";
        for (char c : arr) {
            if (capitalizeNext && Character.isLetter(c)) {
                phrase += Character.toUpperCase(c);
                capitalizeNext = false;
                continue;
            } else if (Character.isWhitespace(c)) {
                capitalizeNext = true;
            }
            phrase += c;
        }
        return phrase;
    }

    /*Plans*/

    private void getPlans() {
        PlanApiInterface planApiInterface = ApiClient.createService(PlanApiInterface.class, this);
        Call<PlansResponse> plansResponse = planApiInterface.getPlanResponse(userId, catName, token, Constants.TAG);
        plansResponse.enqueue(new Callback<PlansResponse>() {
            @Override
            public void onResponse(Call<PlansResponse> call, Response<PlansResponse> response) {
                if (response.body() != null) {
                    if (response.body().getSuccess() == 1) {
                        planData.clear();
                        if (response.body().getPlanData() != null && response.body().getPlanData().size() > 0) {
                            planData.addAll(response.body().getPlanData());
                            if (planStatus == 0) {
                                showPlans(planData);
                            } else {
                                Toast.makeText(VideoDetailActivity.this, "Download Now", Toast.LENGTH_SHORT).show();
                            }
                        }
                    } else {
                        Toast.makeText(VideoDetailActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(VideoDetailActivity.this, "No Data Found", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<PlansResponse> call, Throwable t) {
                if (t instanceof IOException) {
                    Toast.makeText(VideoDetailActivity.this, Constants.NETWORK_FAILURE, Toast.LENGTH_SHORT).show();
                    // logging probably not necessary
                } else {
                    Toast.makeText(VideoDetailActivity.this, Constants.SOMETHING_WENT_WRONG, Toast.LENGTH_SHORT).show();
                    // todo log to some central bug tracking service
                }
            }
        });
    }

    private void showPlans(ArrayList<PlanData> planData) {
        final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(VideoDetailActivity.this, R.style.SheetDialog);
        View sheetView = LayoutInflater.from(VideoDetailActivity.this).inflate(R.layout.layout_bottom_plans, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.show();
        rvBottomPlans = sheetView.findViewById(R.id.rvBottomPlans);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(VideoDetailActivity.this, 2);
        rvBottomPlans.setLayoutManager(gridLayoutManager);
        PlanAdapter planAdapter = new PlanAdapter(VideoDetailActivity.this, VideoDetailActivity.this, planData);
        rvBottomPlans.setAdapter(planAdapter);
    }

    /*Create a Collection */
    private void createCollection(String edtCollection) {
        pbarCollection.setVisibility(View.VISIBLE);
        collectionName = edtCollection;
        CollectionApiInterface createCollectionApiInterface = ApiClient.createService(CollectionApiInterface.class, this);
        Call<CreateCollectionResponse> createCollectionResponse = createCollectionApiInterface.getCreateCollectionResponse(userId, catName, collectionName, token, Constants.TAG);
        createCollectionResponse.enqueue(new Callback<CreateCollectionResponse>() {
            @Override
            public void onResponse(Call<CreateCollectionResponse> call, Response<CreateCollectionResponse> response) {
                pbarCollection.setVisibility(View.GONE);
                if (response.body() != null) {
                    if (response.body().getSuccess() == 0) {
                        Toast.makeText(VideoDetailActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                    if (response.body().getSuccess() == 1) {
                        collectionId = response.body().getCollectionId();
                        insertItem(collectionId);
                    }
                    if (response.body().getSuccess() == 2) {
                        Toast.makeText(VideoDetailActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(VideoDetailActivity.this, Constants.NETWORK_ERROR, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CreateCollectionResponse> call, Throwable t) {
                pbarCollection.setVisibility(View.GONE);
                if (t instanceof IOException) {
                    Toast.makeText(VideoDetailActivity.this, Constants.NETWORK_FAILURE, Toast.LENGTH_SHORT).show();
                    // logging probably not necessary
                } else {
                    Toast.makeText(VideoDetailActivity.this, Constants.SOMETHING_WENT_WRONG, Toast.LENGTH_SHORT).show();
                    // todo log to some central bug tracking service
                }
            }
        });
    }

    /*UserList ShowIn in Dialog*/
    private void getUserList() {
        pbarCollection.setVisibility(View.VISIBLE);
        CollectionApiInterface collectionApiInterface = ApiClient.createService(CollectionApiInterface.class, this);
        Call<UserCollectionResponse> userCollectionResponse = collectionApiInterface.getUserCollectionResponse(userId, catName, token, Constants.TAG);
        userCollectionResponse.enqueue(new Callback<UserCollectionResponse>() {
            @Override
            public void onResponse(Call<UserCollectionResponse> call, Response<UserCollectionResponse> response) {
                pbarCollection.setVisibility(View.GONE);
                if (response.body() != null) {
                    if (response.body().getSuccess() == 1) {
                        userCollectionListData.clear();
                        if (response.body().getData() != null && response.body().getData().size() > 0) {
                            userCollectionListData.addAll(response.body().getData());
                            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(VideoDetailActivity.this, LinearLayoutManager.HORIZONTAL, false);
                            rvUsersList.setLayoutManager(linearLayoutManager);
                            UserCollectionListAdpater userCollectionListAdpater = new UserCollectionListAdpater(VideoDetailActivity.this, VideoDetailActivity.this, userCollectionListData, catName);
                            rvUsersList.setAdapter(userCollectionListAdpater);
                            userCollectionListAdpater.notifyDataSetChanged();
                        }
                    } else {
                        Toast.makeText(VideoDetailActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<UserCollectionResponse> call, Throwable t) {
                pbarCollection.setVisibility(View.GONE);
                if (t instanceof IOException) {
                    Toast.makeText(VideoDetailActivity.this, Constants.NETWORK_FAILURE, Toast.LENGTH_SHORT).show();
                    // logging probably not necessary
                } else {
                    Toast.makeText(VideoDetailActivity.this, Constants.SOMETHING_WENT_WRONG, Toast.LENGTH_SHORT).show();
                    // todo log to some central bug tracking service
                }
            }
        });
    }

    /*InsertItemInCollection*/
    private void insertItem(int collectionId) {
        pbarCollection.setVisibility(View.VISIBLE);
        CollectionApiInterface collectionApiInterface = ApiClient.createService(CollectionApiInterface.class, this);
        Call<InsertCollectionResposne> insertCollectionResposne = collectionApiInterface.getInsertResponse(userId, itemId, catName, collectionId, token, Constants.TAG);
        insertCollectionResposne.enqueue(new Callback<InsertCollectionResposne>() {
            @Override
            public void onResponse(Call<InsertCollectionResposne> call, Response<InsertCollectionResposne> response) {
                pbarCollection.setVisibility(View.GONE);
                if (response.body() != null) {
                    if (response.body().getSuccess() == 0) {
                        Toast.makeText(VideoDetailActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                    if (response.body().getSuccess() == 1) {
                        Toast.makeText(VideoDetailActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                        getVidDetails(itemId);
                    }
                    if (response.body().getSuccess() == 2) {
                        Toast.makeText(VideoDetailActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(VideoDetailActivity.this, Constants.NETWORK_ERROR, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<InsertCollectionResposne> call, Throwable t) {
                pbarCollection.setVisibility(View.GONE);
                if (t instanceof IOException) {
                    Toast.makeText(VideoDetailActivity.this, Constants.NETWORK_FAILURE, Toast.LENGTH_SHORT).show();
                    // logging probably not necessary
                } else {
                    Toast.makeText(VideoDetailActivity.this, Constants.SOMETHING_WENT_WRONG, Toast.LENGTH_SHORT).show();
                    // todo log to some central bug tracking service
                }
            }
        });

    }

    /*Related CallBack for Reloading Data*/
    public void relatedCallBackFromAdpater(String videoId) {
        itemId = videoId;
        try {
            if (Utils.checkInternetConnection(getApplicationContext())) {
                simpleExoPlayer.stop(true);
                simpleExoPlayer.clearVideoSurface();
                simpleExoPlayer.setVideoSurfaceView((SurfaceView) playerView.getVideoSurfaceView());
                simpleExoPlayer.setPlayWhenReady(true);
                getVidDetails(videoId);
            } else {
                Toast.makeText(this, Constants.NETWORK_ERROR, Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            pb_loading.setVisibility(View.GONE);
            e.printStackTrace();
        }
    }

    /*UserDataListAdpaterCallBack*/
    @Override
    public void onItemClickCallBack(int collectionId) {
        insertItem(collectionId);
        dialog.dismiss();
    }

    /*PlansClick*/
    @Override
    public void PlanItemClick(PlanData planData) {

    }


    /*ExoplayerCode Here*/

    /*Here we can set Exoplayer player for video and Floating player also
     * So First we have to set floating player
     * Then we have to set Exoplayer in thi Activity*/

    @Override
    public void onStart() {
        super.onStart();
        if (Util.SDK_INT >= 24) {
            initializePlayer();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        // hideSystemUi();
        active = true;
        if (Util.SDK_INT > 23) {
            initializePlayer();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (Util.SDK_INT < 24) {
            releasePlayer();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (Util.SDK_INT >= 24) {
            releasePlayer();
        }
        finishAndRemoveTask();
    }     /*Exoplayer Methods*/


    private void playVideo(String videoUrl) {
        Uri uri = Uri.parse(videoUrl);
        simpleExoPlayer.seekTo(resumePosition);
        if (buildMediaSource(uri, extension) != null) {
            mediaSource = buildMediaSource(uri, extension);
            boolean haveResumePosition = resumeWindow != C.INDEX_UNSET;
            if (haveResumePosition) {
                simpleExoPlayer.seekTo(resumeWindow, resumePosition);
            }
            simpleExoPlayer.prepare(mediaSource, !haveResumePosition, false);
            simpleExoPlayer.seekTo(currentWindow, playbackPosition);
            inErrorState = false;
            updateimageVisibilities();
        }
    }

    private void updateimageVisibilities() {
//        settingview.setVisibility(View.GONE);
    }

    private void initializePlayer() {
        boolean needNewPlayer = simpleExoPlayer == null;
        if (needNewPlayer) {
            TrackSelection.Factory adaptiveTrackSelectionFactory = new AdaptiveTrackSelection.Factory();
            trackSelector = new DefaultTrackSelector(adaptiveTrackSelectionFactory);
            simpleExoPlayer = ExoPlayerFactory.newSimpleInstance(VideoDetailActivity.this, trackSelector);
            audioReactor = new AudioReactor(getApplicationContext(), simpleExoPlayer);
            maxVolume = audioReactor.getMaxVolume();
            lastSeenTrackGroupArray = null;
            simpleExoPlayer.addListener(new PlayerEventListener());
            simpleExoPlayer.setPlayWhenReady(shouldAutoPlay);
            simpleExoPlayer.seekTo(currentWindow, playbackPosition);
            playerView.setFastForwardIncrementMs(5000);
            playerView.setRewindIncrementMs(5000);
            playerView.setPlayer(simpleExoPlayer);
            playerView.setPlaybackPreparer(this);
            playerView.setUseController(true);
            /*This is for gestures to open volume and brightness */
           /* playerView.addOnLayoutChangeListener((view, l, t, r, b, ol, ot, or, ob) -> {
                if (l != ol || t != ot || r != or || b != ob) {
                    int width = r - l, height = b - t;
                    maxGestureLength = (int) (Math.min(width, height) * MAX_GESTURE_LENGTH);

                    if (DEBUG) Log.d(TAG, "maxGestureLength = " + maxGestureLength);

                    brPG.setMax(maxGestureLength);
                    vPG.setMax(maxGestureLength);
                    setInitialGestureValues();
                }
            });
            setUpGestureControls();*/
        }
    }

    private void setInitialGestureValues() {
        if (audioReactor != null) {
            final float currentVolumeNormalized = (float) audioReactor.getVolume() / audioReactor.getMaxVolume();
            vPG.setProgress((int) (vPG.getMax() * currentVolumeNormalized));
        }
        if (playerPrefrence != null) {
            if (playerPrefrence.getFloat(Constants.BRIGHTNESS_FLOAT_PREF) != -100f) {
                brPG.setProgress((int) (brPG.getMax() * playerPrefrence.getFloat(Constants.BRIGHTNESS_FLOAT_PREF)));
            }
        }

    }

    private void setUpGestureControls() {
        playerView.setOnTouchListener(new ExVidPlayerGestureListener(VideoDetailActivity.this, playerView));
        playerView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                playerView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                playerView.getHeight(); //height is ready
                screenWidth = playerView.getWidth();
                screenHeigth = playerView.getHeight();
            }
        });
    }


    private void releasePlayer() {
        if (simpleExoPlayer != null) {
            playWhenReady = simpleExoPlayer.getPlayWhenReady();
            playbackPosition = simpleExoPlayer.getCurrentPosition();
            currentWindow = simpleExoPlayer.getCurrentWindowIndex();
            shouldAutoPlay = simpleExoPlayer.getPlayWhenReady();
            updateResumePosition();
            simpleExoPlayer.release();
            simpleExoPlayer = null;
            trackSelector = null;
        }
    }

    private MediaSource buildMediaSource(Uri uri, String overrideExtension) {
        @C.ContentType int type = TextUtils.isEmpty(overrideExtension) ? Util.inferContentType(uri)
                : Util.inferContentType("." + overrideExtension);
        switch (type) {
            case C.TYPE_HLS:
                DataSource.Factory mediaDataSourceFactory = new DefaultHttpDataSourceFactory("ua", BANDWIDTH_METER);
                return new HlsMediaSource.Factory(mediaDataSourceFactory).createMediaSource(uri);
            case C.TYPE_DASH:
                return new DashMediaSource.Factory(new DefaultHttpDataSourceFactory("ua", BANDWIDTH_METER)).createMediaSource(uri);
            case C.TYPE_SS:
                return new SsMediaSource.Factory(new DefaultHttpDataSourceFactory("ua", BANDWIDTH_METER)).createMediaSource(uri);
            case C.TYPE_OTHER:
                return new ProgressiveMediaSource.Factory(new DefaultHttpDataSourceFactory("ua", BANDWIDTH_METER)).createMediaSource(uri);
            default: {
                return null;
            }
        }
    }

    private void initFullscreenExoplayerBtn() {
        mFullScreenIcon = controlView.findViewById(R.id.exo_fullscreen_icon);
        mFullScreenButton = controlView.findViewById(R.id.exo_fullscreen_button);
        mFullScreenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mExoPlayerFullscreen) {
                    Log.e("fullscreen", "onclick full");
//                    openFullscreenDialog();
                } else {
//                    closeFullscreenDialog();

                }
            }
        });

    }

    private void openFullscreenDialog() {


    }


    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, DashBoardActivity.class));
        overridePendingTransition(0,
                R.anim.animbackpress
        );
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    @Override
    public void preparePlayback() {

    }

    @Override
    public void onVisibilityChange(int visibility) {

    }


    private class PlayerEventListener implements Player.EventListener {
        @Override
        public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
            switch (playbackState) {
                case Player.STATE_BUFFERING:
                    pb_loading.setVisibility(View.VISIBLE);
                    break;

                case Player.STATE_READY:
                    if (playWhenReady) {
                        pb_loading.setVisibility(View.GONE);
                        Log.e("exoplayer", "play");
                    } else if (playWhenReady) {
                        Log.e("exoplayer", "buffering complete");
                    } else {
                    }
                    break;

                case Player.STATE_IDLE:
                    break;

                case Player.STATE_ENDED:
                    showControls();
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                    if (similarData != null && similarData.size() > 0) {
                        videoId = String.valueOf(similarData.get(0).getId());
                        getVidDetails(videoId);
                    } else
                        break;
                    break;
            }
            updateimageVisibilities();
        }

        private void showControls() {
            playerView.showController();
            animateView(settingview, true, DEFAULT_CONTROLS_DURATION, 0);
        }

        private void hideControls() {
            playerView.hideController();
            animateView(settingview, false, DEFAULT_CONTROLS_DURATION, 400);

        }


        @Override
        public void onPositionDiscontinuity(@Player.DiscontinuityReason int reason) {
            if (inErrorState) {
                updateResumePosition();
            }
        }

        @SuppressLint("StringFormatInvalid")
        @Override
        public void onPlayerError(ExoPlaybackException e) {
            String errorString = null;
            if (e.type == ExoPlaybackException.TYPE_RENDERER) {
                Exception cause = e.getRendererException();
            }
            inErrorState = true;
            if (isBehindLiveWindow(e)) {
                clearResumePosition();
                initializePlayer();
            } else {
                updateResumePosition();
                updateimageVisibilities();
                showControls();
            }
        }

        @Override
        @SuppressWarnings("ReferenceEquality")
        public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
            updateimageVisibilities();
            if (trackGroups != lastSeenTrackGroupArray) {
                MappingTrackSelector.MappedTrackInfo mappedTrackInfo = trackSelector.getCurrentMappedTrackInfo();
                if (mappedTrackInfo != null) {
                    if (mappedTrackInfo.getTypeSupport(C.TRACK_TYPE_VIDEO)
                            == MappingTrackSelector.MappedTrackInfo.RENDERER_SUPPORT_UNSUPPORTED_TRACKS) {
                        Toast.makeText(activity, "unsupported video", Toast.LENGTH_SHORT).show();

                    }
                    if (mappedTrackInfo.getTypeSupport(C.TRACK_TYPE_AUDIO)
                            == MappingTrackSelector.MappedTrackInfo.RENDERER_SUPPORT_UNSUPPORTED_TRACKS) {
                        Toast.makeText(activity, "error unsupported audio", Toast.LENGTH_SHORT).show();

                    }
                }
                lastSeenTrackGroupArray = trackGroups;
            }
        }
    }

    private void updateResumePosition() {
        if (simpleExoPlayer != null) {
            resumeWindow = simpleExoPlayer.getCurrentWindowIndex();
            resumePosition = Math.max(0, simpleExoPlayer.getContentPosition());
        }
    }

    private static boolean isBehindLiveWindow(ExoPlaybackException e) {
        if (e.type != ExoPlaybackException.TYPE_SOURCE) {
            return false;
        }
        Throwable cause = e.getSourceException();
        while (cause != null) {
            if (cause instanceof BehindLiveWindowException) {
                return true;
            }
            cause = cause.getCause();
        }
        return false;
    }

    private void clearResumePosition() {
        resumeWindow = C.INDEX_UNSET;
        resumePosition = C.TIME_UNSET;
    }


    private class ExVidPlayerGestureListener extends GestureListener {
        ExVidPlayerGestureListener(Context ctx, View rootview) {
            super(ctx, rootview);
        }

        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {

            if (gestureSeek) {
                seekStatus.setVisibility(View.GONE);

                if (motionEvent.getAction() == 0) {
                    Log.d(TAG, "yes got new index" + currentIndex);


                } else if (motionEvent.getAction() == 1) {
                    if (simpleExoPlayer != null) {
                        try {
                            Log.d(TAG, gestureSeekPosition + "yes got ne2222w index" + gestureSeekIndex);

                            simpleExoPlayer.seekTo(gestureSeekIndex, gestureSeekPosition);
                        } catch (Exception e) {
                            Log.d(TAG, "yes error");
                        }
                    }
                    play.animateToState(PlayIconDrawable.IconState.PAUSE);
                    simpleExoPlayer.setPlayWhenReady(true);
                    gestureSeek = false;
                }
            }
            return super.onTouch(view, motionEvent);
        }

        @Override
        public void onTap() {
            playerView.setControllerAutoShow(true);
            if (controlView.getVisibility() == View.VISIBLE) {
                playerView.hideController();
            } else {
                playerView.showController();
            }
        }

        @Override
        public void onHorizontalScroll(MotionEvent event, float delta) {
        }


        @Override
        public void onSwipeRight() {

        }

        @Override
        public void onSwipeLeft() {


        }

        @Override
        public void onSwipeBottom() {
//            floatingPlayer();
        }

        @Override
        public void onSwipeTop() {
        }

        @Override
        public void brightness(int value) {
            Log.d("Brigthnesss", value + ">>>");
            if (!gestureSeek) {
                brPG.incrementProgressBy(value);
                float currentProgressPercent =
                        (float) brPG.getProgress() / maxGestureLength;
                WindowManager.LayoutParams layoutParams = getWindow().getAttributes();
                layoutParams.screenBrightness = currentProgressPercent;
                getWindow().setAttributes(layoutParams);
                if (playerPrefrence != null) {
                    playerPrefrence.saveFloatData(Constants.BRIGHTNESS_FLOAT_PREF, currentProgressPercent);

                }
                if (DEBUG)
                    Log.d(TAG, "onScroll().brightnessControl, currentBrightness = " + currentProgressPercent);

                final int resId =
                        currentProgressPercent < 0.25 ? R.drawable.ic_brightness_low_white_72dp
                                : currentProgressPercent < 0.75 ? R.drawable.ic_brightness_medium_white_72dp
                                : R.drawable.ic_brightness_high_white_72dp;

                brIV.setImageDrawable(
                        AppCompatResources.getDrawable(getApplicationContext(), resId)
                );

                if (brView.getVisibility() != View.VISIBLE) {
                    animateView(brView, SCALE_AND_ALPHA, true, 200);
                }
            }
        }

        @Override
        public void volume(int value) {
            //Log.d(TAG, value + ">>>>>");
            if (!gestureSeek) {
                vPG.incrementProgressBy(value);
                float currentProgressPercent =
                        (float) vPG.getProgress() / maxGestureLength;
                int currentVolume = (int) (maxVolume * currentProgressPercent);
                if (audioReactor != null) {
                    Log.d(TAG, currentVolume + ">>>>chappa" + maxVolume);
                    audioReactor.setVolume(currentVolume);
                }

                if (DEBUG)
                    Log.d(TAG, "oncom.kog.activityScroll().volumeControl, currentVolume = " + currentVolume);

                final int resId =
                        currentProgressPercent <= 0 ? R.drawable.ic_volume_off_white_72dp
                                : currentProgressPercent < 0.25 ? R.drawable.ic_volume_mute_white_72dp
                                : currentProgressPercent < 0.75 ? R.drawable.ic_volume_down_white_72dp
                                : R.drawable.ic_volume_up_white_72dp;

                vIV.setImageDrawable(
                        AppCompatResources.getDrawable(getApplicationContext(), resId)
                );

                if (volumeView.getVisibility() != View.VISIBLE) {
                    animateView(volumeView, SCALE_AND_ALPHA, true, 200);
                }
                if (brView.getVisibility() == View.VISIBLE) {
                    brView.setVisibility(View.GONE);
                }
            }
        }

        @Override
        public void onScrollEnd() {
            onScrollOver();
        }

    }

    private void onScrollOver() {
        if (DEBUG) Log.d(TAG, "onScrollEnd() called");
        if (volumeView.getVisibility() == View.VISIBLE) {
            animateView(volumeView, SCALE_AND_ALPHA, false, 200, 200);
        }
        if (brView.getVisibility() == View.VISIBLE) {
            animateView(brView, SCALE_AND_ALPHA, false, 200, 200);
        }
    }
}

