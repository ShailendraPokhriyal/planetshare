package com.planetshare.activity.imgvidcollection;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.planetshare.activity.R;
import com.planetshare.adapters.collection.TabAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ImgVidCollection extends AppCompatActivity {


    @BindView(R.id.tabCollec)
    TabLayout tabMode;
    @BindView(R.id.vpCollec)
    ViewPager slider;

    private TabAdapter tabAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_img_vid_collection);
        ButterKnife.bind(this);


//        TAB MODES ELEMENT
        tabMode.addTab(tabMode.newTab().setText("Image"));
        tabMode.addTab(tabMode.newTab().setText("Video"));

        tabMode.setTabGravity(TabLayout.GRAVITY_FILL);
        tabAdapter = new TabAdapter(this, getSupportFragmentManager(), tabMode.getTabCount());
        slider.setAdapter(tabAdapter);

        slider.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabMode));
        tabMode.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                slider.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

}


