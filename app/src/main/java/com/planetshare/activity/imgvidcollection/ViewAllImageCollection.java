package com.planetshare.activity.imgvidcollection;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.planetshare.activity.R;
import com.planetshare.adapters.collection.ViewAllCollectionAdapter;
import com.planetshare.localstorage.SessionManager;
import com.planetshare.models.collection.usercollection.Data;
import com.planetshare.models.collection.usercollection.UserCollectionListResponse;
import com.planetshare.retrofit.ApiClient;
import com.planetshare.retrofit.GetUserCollectionApiInterface;
import com.planetshare.utils.Constants;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewAllImageCollection extends AppCompatActivity {

    @BindView(R.id.ivCollback)
    ImageView ivCollback;
    @BindView(R.id.tvViewAll)
    TextView tvViewAll;
    @BindView(R.id.rvViewAllColl)
    RecyclerView rvViewAllColl;
    @BindView(R.id.pbHome)
    ProgressBar pbHome;

    ArrayList<Data> userCollectionResponseArrayList;
    ViewAllCollectionAdapter viewAllCollectionAdapter;

    String collec_id, Collection_Name;
    public SessionManager sessionManager;
    private String token, collection_type;
    private int userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_all_collection);
        ButterKnife.bind(this);

        initializeViews();

        ivCollback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        getCollectionImage();

    }

    private void initializeViews() {
        sessionManager = new SessionManager(this);
        token = sessionManager.getToken();
        userId = sessionManager.getUserId();


        if (getIntent().getStringExtra("collec_id") != null) {
            collec_id = getIntent().getStringExtra("collec_id");
        }
        if (getIntent().getStringExtra("collection_type") != null) {
            collection_type = getIntent().getStringExtra("collection_type");
        }
        if (getIntent().getStringExtra("Collection_Name") != null) {
            Collection_Name = getIntent().getStringExtra("Collection_Name");
        }

        userCollectionResponseArrayList = new ArrayList<>();

        tvViewAll.setText(Collection_Name);
    }

    private void getCollectionImage() {
        pbHome.setVisibility(View.VISIBLE);

        GetUserCollectionApiInterface getUserCollectionApiInterface = ApiClient.createService(GetUserCollectionApiInterface.class, this);
        Call<UserCollectionListResponse> userCollectionListResponseCall = getUserCollectionApiInterface.getUserCollectionApiResponse(46, collection_type, Integer.parseInt(collec_id), Constants.TAG, token);
        userCollectionListResponseCall.enqueue(new Callback<UserCollectionListResponse>() {
            @Override
            public void onResponse(Call<UserCollectionListResponse> call, Response<UserCollectionListResponse> response) {
                pbHome.setVisibility(View.GONE);

                if (response.body() != null) {
                    if (response.body().getSuccess() == 1) {
                        userCollectionResponseArrayList.clear();

                        if (response.body().getData() != null && response.body().getData().size() > 0) {

                            userCollectionResponseArrayList.addAll(response.body().getData());

//                            Toast.makeText(ViewAllImageCollection.this, "Success", Toast.LENGTH_SHORT).show();

                            setupCollecAdapter();
                        }
                    } else {
                        Toast.makeText(ViewAllImageCollection.this, Constants.SOMETHING_WENT_WRONG, Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<UserCollectionListResponse> call, Throwable t) {
                pbHome.setVisibility(View.GONE);
                if (t instanceof IOException) {
                    Toast.makeText(ViewAllImageCollection.this, Constants.NETWORK_FAILURE, Toast.LENGTH_SHORT).show();
                    // logging probably not necessary
                } else {
                    Toast.makeText(ViewAllImageCollection.this, Constants.SOMETHING_WENT_WRONG, Toast.LENGTH_SHORT).show();
                    // todo log to some central bug tracking service
                }
            }
        });
    }

    private void setupCollecAdapter() {
        GridLayoutManager layoutManager = new GridLayoutManager(this, 2, LinearLayoutManager.VERTICAL, false);
        rvViewAllColl.setLayoutManager(layoutManager);
        viewAllCollectionAdapter = new ViewAllCollectionAdapter(this, userCollectionResponseArrayList,collection_type);
        rvViewAllColl.setAdapter(viewAllCollectionAdapter);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
