package com.planetshare.adapters.collection;

import android.content.Context;
import android.content.Intent;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.planetshare.activity.R;
import com.planetshare.activity.imgvidcollection.ViewAllImageCollection;
import com.planetshare.models.collection.getusercollection.UserData;

import java.util.ArrayList;

public class ImgVidCollecAdapter extends RecyclerView.Adapter<ImgVidCollecAdapter.MyViewHolder> {
    private Context context;
    private ArrayList<UserData> userCollectionListData;
    //    private ArrayList<UserData> data;
    private String collection_type;

    public ImgVidCollecAdapter(Context context, ArrayList<UserData> userCollectionListData, String collection_type/* ArrayList<UserData> data*/) {
        this.context = context;
        this.userCollectionListData = userCollectionListData;
        this.collection_type = collection_type;
//        this.data = data;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.collection_group_card, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        DisplayMetrics displaymetrics = context.getResources().getDisplayMetrics();
        int width_home = displaymetrics.widthPixels;
        int x, y;
        x = (int) (width_home / 2);
        y = (int) (x / 1.5);
        holder.ivThumbnail.setLayoutParams(new FrameLayout.LayoutParams(x, y));

        holder.tvSize.setText(String.valueOf(" + "+ userCollectionListData.get(position).getCartListCount()));
        holder.tvCollecName.setText(userCollectionListData.get(position).getCollectionName());

        String collec_id = String.valueOf(userCollectionListData.get(position).getId());
        String Collection_Name = userCollectionListData.get(position).getCollectionName();


        holder.cardCollec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, ViewAllImageCollection.class);
                i.putExtra("collec_id", collec_id);
                i.putExtra("collection_type", collection_type);
                i.putExtra("Collection_Name", Collection_Name);
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return userCollectionListData.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView ivThumbnail;
        TextView tvCollecName, tvSize;
        RelativeLayout cardCollec;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            cardCollec = itemView.findViewById(R.id.cardCollec);
            ivThumbnail = itemView.findViewById(R.id.ivThumbnail);
            tvCollecName = itemView.findViewById(R.id.tvCollecName);
            tvSize = itemView.findViewById(R.id.tvSize);
        }
    }
}
