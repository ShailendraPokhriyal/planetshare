package com.planetshare.adapters.collection;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.planetshare.fragments.vidimgcollection.ImageCollectionFragment;
import com.planetshare.fragments.vidimgcollection.VideoCollectionFragment;

public class TabAdapter extends FragmentPagerAdapter {
    private Context context;
    private int totalTabs;

    public TabAdapter(Context context, FragmentManager fragmentManager, int totalTabs) {
        super(fragmentManager);
        this.context = context;
        this.totalTabs = totalTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new ImageCollectionFragment();
            case 1:
                return new VideoCollectionFragment();
            /*default:
                new UploadFragmentTab();*/
        }
        return null;
    }

    @Override
    public int getCount() {
        return totalTabs;
    }
}
