package com.planetshare.adapters.collection;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.planetshare.activity.R;
import com.planetshare.models.collection.usercollection.Data;

import java.util.ArrayList;

public class ViewAllCollectionAdapter extends RecyclerView.Adapter<ViewAllCollectionAdapter.MyViewHolder> {
    private Context context;
    private ArrayList<Data> dataArrayList;
    private String collection_type;

    public ViewAllCollectionAdapter(Context context, ArrayList<Data> dataArrayList, String collection_type) {
        this.context = context;
        this.dataArrayList = dataArrayList;
        this.collection_type = collection_type;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_view_all_collection_card, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        if (dataArrayList != null && dataArrayList.size() > 0) {
            DisplayMetrics displaymetrics = context.getResources().getDisplayMetrics();
            int width_home = displaymetrics.widthPixels;
            int x, y;
            x = (int) (width_home / 2);
            y = (int) (x / 1);
            if (collection_type.equalsIgnoreCase("image")) {
                holder.ivViewAll.setLayoutParams(new FrameLayout.LayoutParams(x, y));
                if (dataArrayList.get(position).getImageItemInfo().getLargeThumb() != null) {
                    Glide.with(context).load(dataArrayList.get(position).getImageItemInfo().getLargeThumb()).into(holder.ivViewAll);
                }
            } else {
                holder.ivViewAll.setLayoutParams(new FrameLayout.LayoutParams(x, y));
                if (dataArrayList.get(position).getVideoItemInfo().getLargeThumb() != null) {
                    Glide.with(context).load(dataArrayList.get(position).getVideoItemInfo().getLargeThumb()).into(holder.ivViewAll);
                }
            }
        } else {
            Toast.makeText(context, "No Data Found", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public int getItemCount() {
        return dataArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView ivViewAll;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ivViewAll = itemView.findViewById(R.id.ivViewAll);
        }
    }
}
