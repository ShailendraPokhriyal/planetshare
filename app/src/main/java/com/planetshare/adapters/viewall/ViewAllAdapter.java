package com.planetshare.adapters.viewall;

import android.content.Context;
import android.content.Intent;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.planetshare.activity.R;
import com.planetshare.activity.imgvideodetail.ImgDetailActivity;
import com.planetshare.activity.imgvideodetail.VideoDetailActivity;
import com.planetshare.models.viewall.ViewAllListing;
import com.planetshare.utils.Constants;

import java.util.ArrayList;

public class ViewAllAdapter extends RecyclerView.Adapter<ViewAllAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<ViewAllListing> viewAllListing;
    private String catName;

    public ViewAllAdapter(Context context, ArrayList<ViewAllListing> viewAllListing, String catName) {
        this.context = context;
        this.viewAllListing = viewAllListing;
        this.catName = catName;
    }

    @NonNull
    @Override
    public ViewAllAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.viewall_adapter_item, parent, false);
        return new ViewAllAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewAllAdapter.MyViewHolder holder, int position) {
        DisplayMetrics displaymetrics = context.getResources().getDisplayMetrics();
        int width = displaymetrics.widthPixels;
        int x = (width / 2);
        int y = (int) (x / 1.3);
        holder.img_thumbnail.setLayoutParams(new FrameLayout.LayoutParams(x, y));
        Glide.with(context).load(viewAllListing.get(position).getLargeThumb()).into(holder.img_thumbnail);
        holder.cvParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (catName.equalsIgnoreCase("video")) {
                    Intent intent = new Intent(context, VideoDetailActivity.class);
                    intent.putExtra("catName", catName);
                    intent.putExtra("titleName", viewAllListing.get(holder.getAdapterPosition()).getTitle());
                    intent.putExtra("videoId", String.valueOf(viewAllListing.get(holder.getAdapterPosition()).getId()));
                    context.startActivity(intent);
                } else if (catName.equalsIgnoreCase("image")) {
                    Intent intent = new Intent(context, ImgDetailActivity.class);
                    intent.putExtra("catName", catName);
                    intent.putExtra("titleName", viewAllListing.get(holder.getAdapterPosition()).getTitle());
                    intent.putExtra("imageId", String.valueOf(viewAllListing.get(holder.getAdapterPosition()).getId()));
                    context.startActivity(intent);
                } else {
                    Toast.makeText(context, Constants.SOMETHING_WENT_WRONG, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return viewAllListing.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView img_thumbnail, tv_pre;
        private TextView tv_duration, tvName, tvPrice;
        private CardView cvParent;
        private LinearLayout llbtmname;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            llbtmname = itemView.findViewById(R.id.llbtmname);
            img_thumbnail = itemView.findViewById(R.id.img_thumb_nail);
            tv_duration = itemView.findViewById(R.id.tv_duration);
            tvName = itemView.findViewById(R.id.tvName);
            cvParent = itemView.findViewById(R.id.cvParent);
            tvPrice = itemView.findViewById(R.id.tvPrice);
            tvPrice.setVisibility(View.GONE);
            tvName.setVisibility(View.GONE);
            llbtmname.setVisibility(View.GONE);
        }
    }
}
