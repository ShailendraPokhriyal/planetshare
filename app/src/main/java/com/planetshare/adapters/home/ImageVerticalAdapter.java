package com.planetshare.adapters.home;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.planetshare.activity.R;
import com.planetshare.activity.viewall.ViewAllActivity;
import com.planetshare.models.home.ImageCat;

import java.util.ArrayList;

public class ImageVerticalAdapter extends RecyclerView.Adapter<ImageVerticalAdapter.MyViewHolder> {
    private Context context;
    private ArrayList<ImageCat> imageList;

    public ImageVerticalAdapter(Context context, ArrayList<ImageCat> imageList) {
        this.context = context;
        this.imageList = imageList;
    }

    @NonNull
    @Override
    public ImageVerticalAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.vertical_adapter_item, parent, false);
        return new ImageVerticalAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ImageVerticalAdapter.MyViewHolder holder, int position) {
        if (imageList.get(holder.getAdapterPosition()).getGetImageCount().size() > 0) {
            if (imageList.get(position).getName().equalsIgnoreCase("education") || imageList.get(position).getName().equalsIgnoreCase("christmas")) {
                holder.tvCategoryName.setText(imageList.get(position).getName());
                StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, LinearLayoutManager.HORIZONTAL);
                holder.rvVertical.setLayoutManager(staggeredGridLayoutManager);

            } else if (imageList.get(position).getName().equalsIgnoreCase("celebrations")) {
                holder.tvCategoryName.setText(imageList.get(position).getName());
                LinearLayoutManager horizontalLayoutmanager1 = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
                holder.rvVertical.setLayoutManager(horizontalLayoutmanager1);
                horizontalLayoutmanager1.setSmoothScrollbarEnabled(true);
            } else {
                holder.tvCategoryName.setText(imageList.get(position).getName());
                holder.tvCategoryName.setText(imageList.get(position).getName());
                LinearLayoutManager horizontalLayoutmanager1 = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
                holder.rvVertical.setLayoutManager(horizontalLayoutmanager1);
                horizontalLayoutmanager1.setSmoothScrollbarEnabled(true);
            }
            holder.tvCategoryName.setText(imageList.get(position).getName());
            holder.rvVertical.setNestedScrollingEnabled(false);
            holder.rvVertical.setItemAnimator(new DefaultItemAnimator());
            ImageHorizontalAdapter imageHorizontalAdapter = new ImageHorizontalAdapter(context, imageList.get(holder.getAdapterPosition()).getGetImageCount(), imageList.get(position).getName(), imageList.get(position).getCatName());
            holder.rvVertical.setAdapter(imageHorizontalAdapter);
            holder.tvViewAll.setVisibility(View.VISIBLE);
        } else {
            holder.tvCategoryName.setVisibility(View.GONE);
            holder.rvVertical.setVisibility(View.GONE);
            holder.tvViewAll.setVisibility(View.GONE);
        }

        holder.tvViewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, ViewAllActivity.class);
                i.putExtra("catName", imageList.get(holder.getAdapterPosition()).getCatName());
                i.putExtra("name", imageList.get(holder.getAdapterPosition()).getName());
                i.putExtra("imageCatId", String.valueOf(imageList.get(holder.getAdapterPosition()).getId()));
                i.putExtra("catId", String.valueOf(imageList.get(holder.getAdapterPosition()).getId()));
                context.startActivity(i);
            }
        });
/*//        LinearLayoutManager horizontalLayoutmanager1 = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
//        holder.rvVertical.setLayoutManager(horizontalLayoutmanager1);
       /* holder.rvVertical.setNestedScrollingEnabled(false);
        holder.rvVertical.setItemAnimator(new DefaultItemAnimator());*/

    }

    @Override
    public int getItemCount() {
        return imageList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvCategoryName, tvViewAll;
        public RecyclerView rvVertical;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tvViewAll = itemView.findViewById(R.id.tvViewAll);
            tvCategoryName = itemView.findViewById(R.id.tvCategoryName);
            rvVertical = itemView.findViewById(R.id.rvVertical);
        }
    }
}
/* if (position % 2 == 0) {
                holder.tvCategoryName.setText(imageList.get(position).getName());
                StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, LinearLayoutManager.HORIZONTAL);
                holder.rvVertical.setLayoutManager(staggeredGridLayoutManager);

            }*/