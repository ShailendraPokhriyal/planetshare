package com.planetshare.adapters.home;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.planetshare.activity.R;
import com.planetshare.activity.viewall.ViewAllActivity;
import com.planetshare.models.home.VideoCat;

import java.util.ArrayList;

public class VideoVerticalAdapter extends RecyclerView.Adapter<VideoVerticalAdapter.MyViewHolder> {
    private Context context;
    private ArrayList<VideoCat> videoList;

    public VideoVerticalAdapter(Context context, ArrayList<VideoCat> videoList) {
        this.context = context;
        this.videoList = videoList;
    }

    @NonNull
    @Override
    public VideoVerticalAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.vertical_adapter_item, parent, false);
        return new VideoVerticalAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull VideoVerticalAdapter.MyViewHolder holder, int position) {
        if (videoList.get(holder.getAdapterPosition()).getGetVideoCount().size() > 0) {
            holder.tvCategoryName.setText(videoList.get(position).getName());
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
            holder.rvVertical.setLayoutManager(linearLayoutManager);
            holder.rvVertical.setNestedScrollingEnabled(false);
            holder.rvVertical.setItemAnimator(new DefaultItemAnimator());
            VideoHorizontalAdapter videoHorizontalAdapter = new VideoHorizontalAdapter(context, videoList.get(holder.getAdapterPosition()).getGetVideoCount(), videoList.get(holder.getAdapterPosition()).getCatName());
            holder.rvVertical.setAdapter(videoHorizontalAdapter);
            holder.tvViewAll.setVisibility(View.VISIBLE);
            holder.tvCategoryName.setVisibility(View.VISIBLE);
            holder.rvVertical.setVisibility(View.VISIBLE);
        } else {
            holder.tvCategoryName.setVisibility(View.GONE);
            holder.rvVertical.setVisibility(View.GONE);
            holder.tvViewAll.setVisibility(View.GONE);
        }

        holder.tvViewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, ViewAllActivity.class);
                i.putExtra("catName", videoList.get(holder.getAdapterPosition()).getCatName());
                i.putExtra("name", videoList.get(holder.getAdapterPosition()).getName());
                i.putExtra("imageCatId", String.valueOf(videoList.get(holder.getAdapterPosition()).getId()));
                i.putExtra("catId", String.valueOf(videoList.get(holder.getAdapterPosition()).getId()));
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return videoList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvCategoryName, tvViewAll;
        public RecyclerView rvVertical;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tvViewAll = itemView.findViewById(R.id.tvViewAll);
            tvCategoryName = itemView.findViewById(R.id.tvCategoryName);
            rvVertical = itemView.findViewById(R.id.rvVertical);
        }
    }
}
