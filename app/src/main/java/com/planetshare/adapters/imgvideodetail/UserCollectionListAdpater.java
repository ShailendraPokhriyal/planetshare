package com.planetshare.adapters.imgvideodetail;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.planetshare.activity.R;
import com.planetshare.models.collection.getusercollection.UserData;

import java.util.ArrayList;

public class UserCollectionListAdpater extends RecyclerView.Adapter<UserCollectionListAdpater.MyViewHolder> {
    private ArrayList<UserData> userCollectionListData;
    private Context context;
    private String catName;
    private ClickItemCallBack clickItemCallBack;

    public interface ClickItemCallBack {
        void onItemClickCallBack(int collectionId);
    }

    public UserCollectionListAdpater(ClickItemCallBack clickItemCallBack, Context context, ArrayList<UserData> userCollectionListData, String catName) {
        this.context = context;
        this.userCollectionListData = userCollectionListData;
        this.catName = catName;
        this.clickItemCallBack = clickItemCallBack;
    }

    @NonNull
    @Override
    public UserCollectionListAdpater.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.tags_item, parent, false);
        return new UserCollectionListAdpater.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull UserCollectionListAdpater.MyViewHolder holder, int position) {
        holder.tvUserCollectionName.setText(userCollectionListData.get(position).getCollectionName());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickItemCallBack.onItemClickCallBack(userCollectionListData.get(holder.getAdapterPosition()).getId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return userCollectionListData.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView tvUserCollectionName;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tvUserCollectionName = itemView.findViewById(R.id.tvTags);
        }
    }
}
