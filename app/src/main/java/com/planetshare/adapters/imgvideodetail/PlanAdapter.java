package com.planetshare.adapters.imgvideodetail;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.card.MaterialCardView;
import com.planetshare.activity.R;
import com.planetshare.models.plans.PlanData;

import java.util.ArrayList;

public class PlanAdapter extends RecyclerView.Adapter<PlanAdapter.MyViewHolder> {
    Context context;
    ArrayList<PlanData> planData;
    private int selectedPosition = -1;
    PlanItemClick planItemClick;


    public PlanAdapter(PlanItemClick planItemClick, Context context, ArrayList<PlanData> planData) {
        this.context = context;
        this.planData = planData;
        this.planItemClick = planItemClick;
    }

    public interface PlanItemClick {
        void PlanItemClick(PlanData planData);
    }

    @NonNull
    @Override
    public PlanAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.plan_item_adapter, parent, false);
        return new PlanAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull PlanAdapter.MyViewHolder holder, int position) {
        holder.tvPlanName.setText(planData.get(position).getPackName());
        if (planData.get(position).getPopularContent() == 1) {
            holder.tvMostPopular.setText("Most Popular");
            holder.tvMostPopular.setVisibility(View.VISIBLE);
        } else {
            holder.tvMostPopular.setVisibility(View.GONE);
        }
        holder.tvPlanPrice.setText(String.valueOf("\u20B9" + " " + planData.get(position).getPackPrice()));

        holder.tvCountPacks.setText(planData.get(position).getPackCount() + " " + planData.get(position).getPackType() + "s" + " " + "(" +
                planData.get(position).getPackExpireTime() + "Days" + ")");


        if (planData.get(position).getSinglePack() == 1) {
            holder.tvSingelPlan.setText("Singel Pack");
            holder.tvSingelPlan.setVisibility(View.VISIBLE);

        } else {
            holder.tvSingelPlan.setVisibility(View.GONE);
        }

        /*Click on box Behaviour*/
        if (selectedPosition == position) {
            holder.itemView.setBackgroundResource(R.drawable.plan_selected_border);
            holder.tvPlanName.setTextColor(ContextCompat.getColor(context, R.color.blue));
            holder.tvMostPopular.setTextColor(ContextCompat.getColor(context, R.color.blue));
            holder.tvCountPacks.setTextColor(ContextCompat.getColor(context, R.color.black));

        } else {
            holder.itemView.setBackgroundResource(R.drawable.selected_border);
            holder.tvPlanName.setTextColor(ContextCompat.getColor(context, R.color.ligh_blue_clr));
            holder.tvMostPopular.setTextColor(ContextCompat.getColor(context, R.color.ligh_blue_clr));
            holder.tvCountPacks.setTextColor(ContextCompat.getColor(context, R.color.grey_color));
        }
        holder.cvParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                planItemClick.PlanItemClick(planData.get(holder.getAdapterPosition()));
                selectedPosition = holder.getAdapterPosition();
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return planData.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView tvPlanName, tvPlanPrice, tvSingelPlan, tvCountPacks, tvMostPopular;
        private MaterialCardView cvParent;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tvPlanName = itemView.findViewById(R.id.tvPlanName);
            tvPlanPrice = itemView.findViewById(R.id.tvPlanPrice);
            tvMostPopular = itemView.findViewById(R.id.tvMostPopular);
            tvSingelPlan = itemView.findViewById(R.id.tvSingelPlan);
            tvCountPacks = itemView.findViewById(R.id.tvCountPacks);
            cvParent = itemView.findViewById(R.id.cvParent);
        }
    }
}
