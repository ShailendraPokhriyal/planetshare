package com.planetshare.adapters.imgvideodetail;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.planetshare.activity.R;
import com.planetshare.activity.imgvideodetail.VideoDetailActivity;
import com.planetshare.models.imagevideodetails.viddetail.SimilarDataVid;

import java.util.ArrayList;

public class VidAdapter extends RecyclerView.Adapter<VidAdapter.MyViewHolder> {
    Context context;
    private ArrayList<SimilarDataVid> similarData;
    private String catName;

    public VidAdapter(Context context, ArrayList<SimilarDataVid> similarData, String catName) {
        this.context = context;
        this.similarData = similarData;
        this.catName = catName;
    }

    @NonNull
    @Override
    public VidAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.viewall_adapter_item, parent, false);
        return new VidAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull VidAdapter.MyViewHolder holder, int position) {
        DisplayMetrics displaymetrics = context.getResources().getDisplayMetrics();
        int width = displaymetrics.widthPixels;
        int x = (width / 2);
        int y = (int) (x / 1.3);
        holder.img_thumbnail.setLayoutParams(new FrameLayout.LayoutParams(x, y));
        Glide.with(context).load(similarData.get(position).getLargeThumb()).into(holder.img_thumbnail);

        holder.cvParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((VideoDetailActivity) context).relatedCallBackFromAdpater(String.valueOf(similarData.get(position).getId()));
            }
        });
    }

    @Override
    public int getItemCount() {
        return similarData.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView img_thumbnail, tv_pre;
        private TextView tv_duration, tvName, tvPrice;
        private CardView cvParent;
        private LinearLayout llbtmname;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            llbtmname = itemView.findViewById(R.id.llbtmname);
            img_thumbnail = itemView.findViewById(R.id.img_thumb_nail);
            tv_duration = itemView.findViewById(R.id.tv_duration);
            tvName = itemView.findViewById(R.id.tvName);
            cvParent = itemView.findViewById(R.id.cvParent);
            tvPrice = itemView.findViewById(R.id.tvPrice);
            tvPrice.setVisibility(View.GONE);
            tvName.setVisibility(View.GONE);
            llbtmname.setVisibility(View.GONE);
        }
    }
}
